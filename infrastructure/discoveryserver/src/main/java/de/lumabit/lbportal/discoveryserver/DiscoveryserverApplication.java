package de.lumabit.lbportal.discoveryserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableEurekaServer
public class DiscoveryserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiscoveryserverApplication.class, args);
    }

}
