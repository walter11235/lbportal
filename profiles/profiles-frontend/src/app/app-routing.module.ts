import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthResolverService } from "./common/service/resolver/auth-resolver.service";
import { EmployeesComponent } from "./employees/employees.component";
import { EmployeesResolverService } from "./common/service/resolver/employees-resolver.service";
import { EmployeeDetailComponent } from "./employees/employee-detail/employee-detail.component";
import { CatalogComponent } from "./catalog/catalog.component";
import { EmployeeEditComponent } from "./employees/employee-edit/employee-edit.component";
import { ProjectsComponent } from "./projects/projects.component";
import { ProjectsResolverService } from "./common/service/resolver/projects-resolver.service";
import { ProjectEditComponent } from "./projects/project-edit/project-edit.component";
import { AdminGuard } from "./common/service/guard/admin.guard";
import { IsHimGuard } from "./common/service/guard/is-him.guard";
import { EmployeeIndexGuard } from "./common/service/guard/employee-index.guard";
import {ProjectIndexGuard} from "./common/service/guard/project-index.guard";

const appRoutes: Routes = [
  { path: '',
    redirectTo: '/employees',
    pathMatch: 'full'
  },
  { path: 'employees',
    component: EmployeesComponent,
    resolve: [EmployeesResolverService, AuthResolverService],
    children: [
      { path: ':empIndex',
        component: EmployeeDetailComponent,
        canActivate: [EmployeeIndexGuard, IsHimGuard]
      }
    ]
  },
  { path: 'employees/:empIndex/edit',
    component: EmployeeEditComponent,
    resolve: [EmployeesResolverService, AuthResolverService],
    canActivate: [EmployeeIndexGuard, IsHimGuard]
  },
  { path: 'catalog',
    component: CatalogComponent,
    canActivate: [AdminGuard]
  },
  { path: 'employees/:empIndex/edit/projects',
    component: ProjectsComponent,
    resolve: [ProjectsResolverService, AuthResolverService],
    canActivate: [EmployeeIndexGuard, IsHimGuard],
    children: [
      { path: 'new',
        component: ProjectEditComponent
      },
      { path: ':projIndex',
        component: ProjectEditComponent,
        canActivate: [ProjectIndexGuard]
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
  exports: [RouterModule]

})
export class AppRoutingModule {

}
