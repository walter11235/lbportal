import {Component, OnDestroy, OnInit} from '@angular/core';
import { Project } from "../../common/model/project.model";
import { Subscription } from "rxjs";
import { ProjectService } from "../../common/service/project.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import {EmployeeService} from "../../common/service/employee.service";
import {Employee} from "../../common/model/employee.model";

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  index: number;

  projects: Project[];

  employee: Employee;

  subscription: Subscription;

  constructor(private projectService: ProjectService,
              private employeeService: EmployeeService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.index = +params['empIndex'];
          this.employee = this.employeeService.getEmployeeByIndex(this.index);
        }
      );
    this.subscription = this.projectService.projectsChanged
      .subscribe(
        (projects: Project[]) => {
          this.projects = projects;
        }
      );
    this.projects = this.projectService.getProjects();
  }

  onNewProject() {
    this.router.navigate(['new'], {relativeTo: this.activatedRoute});
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
