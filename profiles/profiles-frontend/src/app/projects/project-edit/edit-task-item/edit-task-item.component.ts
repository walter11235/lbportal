import {Component, Input, OnInit} from '@angular/core';
import { Task } from "../../../common/model/task.model";
@Component({
  selector: 'app-edit-task-item',
  templateUrl: './edit-task-item.component.html',
  styleUrls: ['./edit-task-item.component.css']
})
export class EditTaskItemComponent implements OnInit {

  @Input()
  tasks: Task[];

  constructor() { }

  ngOnInit() {
  }

  onDelete(index: number) {
    this.tasks.splice(index,1);
  }

}
