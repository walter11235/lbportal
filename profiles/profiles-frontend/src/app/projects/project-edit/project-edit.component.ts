import { Component, OnInit } from '@angular/core';
import { Project } from "../../common/model/project.model";
import { ProjectService } from "../../common/service/project.service";
import { DataStorageService } from "../../common/service/data-storage.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Task } from "../../common/model/task.model";
import { EmployeeService } from "../../common/service/employee.service";
import { Observable } from "rxjs";
import { MatDialog } from "@angular/material";
import { ErrorMessage } from "../../common/dialog/error-dialog/error-message.interface";
import { ErrorDialogComponent } from "../../common/dialog/error-dialog/error-dialog.component";

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

  projIndex: number;

  empIndex: number;

  empId: number;

  project: Project;

  editMode: Boolean;

  constructor(private projectService: ProjectService,
              private employeeService: EmployeeService,
              private dataStorageService: DataStorageService,
              private dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.parent.params
      .subscribe(
        (params: Params) => {
          this.empIndex = +params['empIndex'];
          this.empId = this.employeeService.getEmployeeByIndex(this.empIndex).id;
        }
      );

    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.projIndex = +params['projIndex'];
          this.editMode = params['projIndex'] != null;
          if (this.editMode) {
          this.project = this.projectService.getProjectByIndex(this.projIndex);
          } else {
            this.project = new Project();
          }
        }
      );
  }

  onAddTaskItem() {
    this.project.tasks.push(new Task());
  }

  onSubmit() {
    let observable : Observable<any>;
    if (this.editMode) {
      observable = this.dataStorageService.updateProject(this.projIndex, this.empId.toString(), this.project);
    } else {
      observable = this.dataStorageService.createProject(this.empId.toString(), this.project);
    }
    observable.subscribe(project => { this.onCancel()},
      errorMessage => {
        this.openErrorDialog(errorMessage);}
        );
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  onDelete() {
    this.dataStorageService.deleteProject(this.projIndex, this.project.id.toString())
      .subscribe(project => {this.onCancel();},
        errorMessage => {
          this.openErrorDialog(errorMessage);}
          );
  }

  openErrorDialog(errorMessage: ErrorMessage): void {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMessage,
      width: '250px',
      height: '100px'
    })
  }

}
