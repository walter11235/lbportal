import { Component, OnInit } from '@angular/core';
import { EmployeeService } from "../../common/service/employee.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Employee } from "../../common/model/employee.model";
import { TechProductService } from "../../common/service/tech-product.service";
import { CareerItem } from "../../common/model/career-item.model";
import { Skill } from "../../common/model/skill.model";
import { DataStorageService } from "../../common/service/data-storage.service";
import { MatDialog } from "@angular/material";
import { ErrorMessage } from "../../common/dialog/error-dialog/error-message.interface";
import { ErrorDialogComponent } from "../../common/dialog/error-dialog/error-dialog.component";

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {

  index: number;
  employee: Employee;

  constructor(private employeeService: EmployeeService,
              private techProductService: TechProductService,
              private dataStorageService: DataStorageService,
              private dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.index = +params['empIndex'];
          this.employee = this.employeeService.getEmployeeByIndex(this.index);
        }
      );
  }

  onAddCareerItem() {
    this.employee.careerItems.push(new CareerItem())
  }

  onAddSkill() {
    this.employee.skills.push(new Skill());
  }

  onSubmit() {
    this.dataStorageService.updateEmployee(this.index, this.employee).subscribe(
      employee => {this.onCancel();},
        errorMessage => {
      this.openErrorDialog(errorMessage);
    }
    );
  }

  onProjects() {
    this.router.navigate(['projects'], {relativeTo: this.activatedRoute});
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  openErrorDialog(errorMessage: ErrorMessage): void {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMessage,
      width: '250px',
      height: '100px'
    })
  }

}
