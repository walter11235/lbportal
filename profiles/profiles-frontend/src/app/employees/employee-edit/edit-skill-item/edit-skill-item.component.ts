import {Component, Input, OnInit} from '@angular/core';
import {Skill} from "../../../common/model/skill.model";

@Component({
  selector: 'app-edit-skill-item',
  templateUrl: './edit-skill-item.component.html',
  styleUrls: ['./edit-skill-item.component.css']
})
export class EditSkillItemComponent implements OnInit {

  @Input()
  skills: Skill[];

  constructor() { }

  ngOnInit() {
  }

  onDelete(index: number) {
    this.skills.splice(index,1);
  }

}
