import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSkillItemComponent } from './edit-skill-item.component';

describe('EditSkillItemComponent', () => {
  let component: EditSkillItemComponent;
  let fixture: ComponentFixture<EditSkillItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSkillItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSkillItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
