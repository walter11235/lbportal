import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCareerItemComponent } from './edit-career-item.component';

describe('EditCareerItemComponent', () => {
  let component: EditCareerItemComponent;
  let fixture: ComponentFixture<EditCareerItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCareerItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCareerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
