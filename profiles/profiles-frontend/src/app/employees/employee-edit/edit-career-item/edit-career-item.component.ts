import {Component, Input, OnInit} from '@angular/core';
import {CareerItem} from "../../../common/model/career-item.model";

@Component({
  selector: 'app-edit-career-item',
  templateUrl: './edit-career-item.component.html',
  styleUrls: ['./edit-career-item.component.css']
})
export class EditCareerItemComponent implements OnInit {

  @Input()
  careerItems: CareerItem[];

  constructor() { }

  ngOnInit() {
  }

  onDelete(index: number) {
    this.careerItems.splice(index,1);
  }

}
