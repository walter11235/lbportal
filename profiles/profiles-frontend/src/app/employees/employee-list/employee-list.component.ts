import { Component, OnInit } from '@angular/core';
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../../common/service/auth.service";
import { EmployeeService } from "../../common/service/employee.service";
import { Employee } from "../../common/model/employee.model";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees: Employee[] = [];

  subscription: Subscription;

  constructor(private employeeService: EmployeeService,
              private route: ActivatedRoute,
              public authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.subscription = this.employeeService.employeesChanged
      .subscribe((employees: Employee[]) => {
        this.employees = employees;
      });
    this.employees = this.employeeService.getEmployees();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
