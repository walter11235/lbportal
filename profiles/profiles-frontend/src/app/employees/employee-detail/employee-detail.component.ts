import { Component, OnInit } from '@angular/core';
import { EmployeeService } from "../../common/service/employee.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import { AuthService } from "../../common/service/auth.service";
import { DomSanitizer } from "@angular/platform-browser";
import { Employee } from "../../common/model/employee.model";

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  empIndex: number;
  employee: Employee;

  constructor(public employeeService: EmployeeService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              public authService: AuthService,
              public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params: Params) => {
      this.empIndex = +params['empIndex'];
      this.employee = this.employeeService.getEmployeeByIndex(this.empIndex);
      })
  }

  getHtmlUrl() {
    return "/api/profiles-backend/html/" + this.employee.id;
  }

  getPdfUrl(temp: string) {
    return "/api/profiles-backend/pdf/" + this.employee.id + "?temp=" + temp ;
  }

  onEdit() {
    this.router.navigate(['edit'], {relativeTo: this.activatedRoute});
  }

  isHim() {
    return this.authService.isHim(this.employee);
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

}
