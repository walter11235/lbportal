import { Component, Input, OnInit } from '@angular/core';
import {TechProductService} from "../../common/service/tech-product.service";
import {TechProduct} from "../../common/model/techproduct/tech-product.model";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.css']
})
export class CatalogListComponent implements OnInit {

  @Input()
  name: string;

  techProducts: TechProduct[];

  newTechProduct: TechProduct = new TechProduct();

  constructor(private techProductService: TechProductService) { }

  ngOnInit() {
    this.techProductService.getAllTechProducts(this.name)
      .subscribe((techProducts: TechProduct[]) => {
        this.techProducts = techProducts;
      });
  }

  create(form: NgForm) {
    this.newTechProduct.name = form.value.techProductName;
    switch (this.name) {
      case 'programminglanguages':
        this.newTechProduct.type = 'ProgrammingLanguage';
        break;
      case 'tools':
        this.newTechProduct.type = 'Tool';
        break;
      case 'technologies':
        this.newTechProduct.type = 'Technology';
        break;
      case 'miscellaneous':
        this.newTechProduct.type = 'Miscellany';
        break;
    }
    this.techProductService.createTechProduct(this.newTechProduct, this.name)
      .subscribe((techProduct: TechProduct) => {
        this.techProducts.push(techProduct);
      });
  }

  delete(techProduct: TechProduct, id: number) {
    this.techProductService.deleteTechProduct(techProduct, this.name)
      .subscribe((response) => {
        this.techProducts.splice(id,1);
      });
  }

}
