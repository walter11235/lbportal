import { Component, Input, OnInit } from '@angular/core';
import { TechProduct } from "../../../common/model/techproduct/tech-product.model";
import { TechProductService } from "../../../common/service/tech-product.service";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-catalog-item',
  templateUrl: './catalog-item.component.html',
  styleUrls: ['./catalog-item.component.css']
})
export class CatalogItemComponent implements OnInit {

  @Input()
  techProduct: TechProduct;

  @Input()
  index: number;

  @Input()
  name: string;

  constructor(private techProductService: TechProductService) { }

  ngOnInit() {
  }

  save(form: NgForm) {
    this.techProductService.saveTechProduct(this.techProduct, this.name)
      .subscribe((techProduct: TechProduct) => {
        this.techProduct = techProduct;
      });
  }

}
