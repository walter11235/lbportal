import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechProductSelectComponent } from './tech-product-select.component';

describe('TechProductSelectComponent', () => {
  let component: TechProductSelectComponent;
  let fixture: ComponentFixture<TechProductSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechProductSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechProductSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
