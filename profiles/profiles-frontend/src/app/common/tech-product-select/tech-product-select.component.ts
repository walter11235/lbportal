import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { TechProductService } from "../service/tech-product.service";
import { TechProduct } from "../model/techproduct/tech-product.model";

@Component({
  selector: 'app-tech-product-select',
  templateUrl: './tech-product-select.component.html',
  styleUrls: ['./tech-product-select.component.css']
})
export class TechProductSelectComponent implements OnInit {

  @Input()
  employeeTechProducts: TechProduct[];

  @Input()
  techProductName: string;

  @Output()
  employeeTechProductsChange = new EventEmitter();

  techProducts: TechProduct[];

  constructor(private techProductService: TechProductService) { }

  ngOnInit() {
    this.techProductService.getAllTechProducts(this.techProductName)
      .subscribe(
        (techProducts: TechProduct[]) => {
          this.techProducts = techProducts;
        }
      )
  }

  equals(objOne, objTwo) {
    if ((objOne !== null && objTwo !== null) &&
      (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined')) {
      return objOne.id === objTwo.id;
    }
  }

  change(newTechProducts) {
    this.employeeTechProductsChange.emit(newTechProducts.value);
  }

}
