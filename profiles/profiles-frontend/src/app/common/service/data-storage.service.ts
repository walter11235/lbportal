import { HttpClient, HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Person } from "../model/person/person.model";
import { catchError, map, tap } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { Employee } from "../model/employee.model";
import { EmployeeService } from "./employee.service";
import { ProjectService } from "./project.service";
import { Project } from "../model/project.model";
import { throwError } from "rxjs";

@Injectable()
export class DataStorageService {

  constructor(private httpClient: HttpClient,
              private authService: AuthService,
              private employeeService: EmployeeService,
              private projectService: ProjectService) {}


  getLoggedInUser() {
    return this.httpClient.get<Person>("/api/usermanagement-backend/person/loggedin_person")
      .pipe(
        tap( person => {
          this.authService.setLoggedInPerson(person);
        }),
        catchError(this.handleError)
      )
  }

  getEmployees() {
    return this.httpClient.get<Employee[]>("/api/profiles-backend/employees/")
      .pipe(
        map(employees => {
          return employees.map(employee => {
            return {...employee, tools: employee.tools ? employee.tools : []};
          });
        }),
        tap(employees => {
          this.employeeService.setEmployees(employees);
        }),
        catchError(this.handleError)
      )
  }

  updateEmployee(index: number, employee: Employee) {
    return this.httpClient.post<Employee>("/api/profiles-backend/employees/save", employee)
      .pipe(
        tap(employee => {
          this.employeeService.updateEmployee(index, employee);
        }),
        catchError(this.handleError)
      );
  }

  updateProject(projIndex: number, empId: string, project: Project ) {
    return this.httpClient.put<Project>("/api/profiles-backend/projects/", project,
      {
                params: new HttpParams().set('empid', empId)
              }
            )
      .pipe(
        tap(project => {
          this.projectService.updateProject(projIndex, project);
        }),
        catchError(this.handleError)
      )
  }

  createProject(empId: string, project: Project) {
    return this.httpClient.post<Project>("/api/profiles-backend/projects/", project,
      {
        params: new HttpParams().set('empid', empId)
      }
      )
      .pipe(
        tap( project => {
          this.projectService.addProject(project);
        }),
        catchError(this.handleError)
      )
  }

  deleteProject(projIndex: number, projId: string) {
    return this.httpClient.delete("/api/profiles-backend/projects/"+ projId)
      .pipe(
        tap(
          project => {this.projectService.deleteProject(projIndex);}
        ),
        catchError(this.handleError)
      );
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = "An unknown error occurred!";
    if (!errorRes.error || !errorRes.error.error) {
      return throwError(errorMessage);
    }
    errorMessage = errorRes.error;
    return throwError(errorMessage);
  }

}
