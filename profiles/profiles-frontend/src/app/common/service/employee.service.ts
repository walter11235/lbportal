import { Employee } from "../model/employee.model";
import { Subject } from "rxjs";

export class EmployeeService {

  private employees: Employee[] = [];

  employeesChanged = new Subject<Employee[]>();

  getEmployees() {
    return this.employees.slice();
  }

  setEmployees(employees: Employee[]) {
    this.employees = employees;
    this.employeesChanged.next(this.employees.slice());
  }

  getEmployeeByIndex(index: number) {
    return this.employees[index];
  }

  addEmployee(employee: Employee) {
    this.employees.push(employee);
    this.employeesChanged.next(this.employees.slice());
  }

  updateEmployee(index: number, updatedEmployee: Employee) {
    this.employees[index] = updatedEmployee;
    this.employeesChanged.next(this.employees.slice());
  }

  deleteEmployee(index: number) {
    this.employees.splice(index, 1);
    this.employeesChanged.next(this.employees.slice());
  }
}
