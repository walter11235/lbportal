import { TechProduct } from "../model/techproduct/tech-product.model";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class TechProductService {

  constructor(private httpClient: HttpClient) {}

  getAllTechProducts(name: string) {
    return this.httpClient.get('/api/profiles-backend/'+ name + '/');
  }

  saveTechProduct(techProduct: TechProduct, name: string) {
    return this.httpClient.put('/api/profiles-backend/' + name + '/' + techProduct.id, techProduct);
  }

  createTechProduct(techProduct: TechProduct, name: string) {
    return this.httpClient.post('/api/profiles-backend/' + name + '/', techProduct);
  }

  deleteTechProduct(techProduct: TechProduct, name: string) {
    return this.httpClient.delete('/api/profiles-backend/' + name + '/' + techProduct.id);
  }
}
