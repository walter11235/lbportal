import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Employee } from "../../model/employee.model";
import { EmployeeService } from "../employee.service";
import { DataStorageService } from "../data-storage.service";
import { Observable } from "rxjs";

@Injectable()
export class EmployeesResolverService implements Resolve<Employee[]> {
  constructor(private employeeService: EmployeeService,
              private dataStorageService: DataStorageService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Employee[]> | Promise<Employee[]> | Employee[] {
    const employees = this.employeeService.getEmployees();

    if (employees.length === 0) {
      return this.dataStorageService.getEmployees();
    } else {
      return employees;
    }
  }
}
