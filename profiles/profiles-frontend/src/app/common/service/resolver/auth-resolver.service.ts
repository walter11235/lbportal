import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../auth.service";
import { Injectable } from "@angular/core";
import { DataStorageService } from "../data-storage.service";
import { Person } from "../../model/person/person.model";

@Injectable()
export class AuthResolverService implements Resolve<Person> {

  constructor(private authService: AuthService,
              private dataStorageService: DataStorageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Person> | Promise<Person> | Person {
    const person = this.authService.getLoggedInPerson();

    if(person === null) {
      return this.dataStorageService.getLoggedInUser();
    } else {
      return person;
    }
  }

}
