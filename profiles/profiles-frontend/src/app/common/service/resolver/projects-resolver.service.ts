import { Project } from "../../model/project.model";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { EmployeeService } from "../employee.service";
import { DataStorageService } from "../data-storage.service";
import { ProjectService } from "../project.service";
import { Injectable } from "@angular/core";
import { exhaustMap, map } from "rxjs/operators";
import { Employee } from "../../model/employee.model";

@Injectable()
export class ProjectsResolverService implements Resolve<Project[]> {

  constructor(private employeeService: EmployeeService,
              private dataStorageService: DataStorageService,
              private projectService: ProjectService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project[]> | Promise<Project[]> | Project[] {
    const currentEmpIndex = route.params.empIndex;
    const employees = this.employeeService.getEmployees();

    if (employees.length === 0) {
      return this.dataStorageService.getEmployees()
        .pipe(
          map((employees: Employee[]) => {
            this.projectService.setProjects(employees[currentEmpIndex].projects);
            return this.employeeService.getEmployeeByIndex(currentEmpIndex).projects;
          })
        );
    } else {
      const projects = this.employeeService.getEmployeeByIndex(currentEmpIndex).projects;
      this.projectService.setProjects(projects);
      return projects;
    }
  }
}
