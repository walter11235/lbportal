import {Project} from "../model/project.model";
import {Subject} from "rxjs";

export class ProjectService {

  private projects: Project[];

  projectsChanged = new Subject<Project[]>();

  getProjects() {
    return this.projects.slice();
  }

  setProjects(projects: Project[]) {
    this.projects = projects;
    this.projectsChanged.next(this.projects.slice());
  }

  getProjectByIndex(index: number) {
    return this.projects[index];
  }

  addProject(project: Project) {
    this.projects.push(project);
    this.projectsChanged.next(this.projects.slice());
  }

  updateProject(index: number, updatedProject: Project) {
    this.projects[index] = updatedProject;
    this.projectsChanged.next(this.projects.slice());
  }

  deleteProject(index: number) {
    this.projects.splice(index,1);
    this.projectsChanged.next(this.projects.slice());
  }

}
