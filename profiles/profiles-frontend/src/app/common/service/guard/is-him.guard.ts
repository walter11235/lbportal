import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree } from "@angular/router";
import { AuthService } from "../auth.service";
import { DataStorageService } from "../data-storage.service";
import { EmployeeService } from "../employee.service";
import { Observable } from "rxjs";
import { exhaustMap, map } from "rxjs/operators";

@Injectable()
export class IsHimGuard implements CanActivate {
  constructor(private authService: AuthService,
              private dataStorageService: DataStorageService,
              private employeeService: EmployeeService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const currentIndex = route.params.empIndex;
    //to confirm if the personResolver and authResolver have been already invoked
    const loggedInPerson = this.authService.getLoggedInPerson();
    const employee = this.employeeService.getEmployeeByIndex(currentIndex);


    if(!loggedInPerson && !employee) {
      return this.dataStorageService.getLoggedInUser().pipe(
        exhaustMap(loggedInPerson => {
          return this.dataStorageService.getEmployees()
            .pipe(
              map(
                employees => {
                  const employee = this.employeeService.getEmployeeByIndex(currentIndex);
                  return this.authService.isHim(employee) || this.authService.isAdmin();
                })
            )
        })
      );
    } else {
      const employee = this.employeeService.getEmployeeByIndex(currentIndex);
      return this.authService.isHim(employee) || this.authService.isAdmin();
    }
  }
}
