import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {ProjectService} from "../project.service";
import {DataStorageService} from "../data-storage.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Employee} from "../../model/employee.model";
import {EmployeeService} from "../employee.service";

@Injectable()
export class ProjectIndexGuard implements CanActivate {
  constructor(private projectService: ProjectService,
              private dataStorageService: DataStorageService,
              private employeeService: EmployeeService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //to confirm if personResolver has already been invoked
    const currentEmpIndex = route.parent.params.empIndex;
    const currentProjIndex = route.params.projIndex;

    const projects = this.projectService.getProjects();

    if(projects.length === 0) {

    } else return !!this.projectService.getProjectByIndex(currentProjIndex);
  }
}
