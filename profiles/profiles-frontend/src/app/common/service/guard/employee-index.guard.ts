import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree } from "@angular/router";
import { EmployeeService } from "../employee.service";
import { DataStorageService } from "../data-storage.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class EmployeeIndexGuard implements CanActivate {
  constructor(private employeeService: EmployeeService,
              private dataStorageService: DataStorageService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //to confirm if employeesResolver has already been invoked
    const currentIndex = route.params.empIndex;
    const employees = this.employeeService.getEmployees();

    if(employees.length === 0) {
      return this.dataStorageService.getEmployees()
        .pipe(
          map(employees => {
            return employees.length >= currentIndex;
          })
        )
    } else return !!this.employeeService.getEmployeeByIndex(currentIndex);
  }
}
