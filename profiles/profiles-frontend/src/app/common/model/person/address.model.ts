import { BaseEntity } from "../base-entity.model";

export class Address extends BaseEntity {

  constructor(
    public addressLine: string = null,
    public state: string = null,
    public city: string = null,
    public zipCode: number = null
  ) {
    super();
  }
}
