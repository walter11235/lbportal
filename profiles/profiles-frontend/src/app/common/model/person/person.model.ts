import {Role} from "./role.model";
import {Address} from "./address.model";
import {BaseEntity} from "../base-entity.model";

export class Person extends BaseEntity {
  constructor(
  public firstName: string = null,
  public lastName: string = null,
  public roles: Role[] = [],
  public birthDate: Date = null,
  public emailAddress: string = null,
  public address: Address = new Address(),
  public imageUrl: string = null) {
    super();
  }
}
