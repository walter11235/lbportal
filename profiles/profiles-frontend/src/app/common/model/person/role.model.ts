import { BaseEntity } from "../base-entity.model";

export class Role extends BaseEntity {
  constructor(
    public name: string = null
  ) {
    super();
  }
}
