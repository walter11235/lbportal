import { BaseEntity } from "./base-entity.model";

export class Skill extends BaseEntity {
  constructor(private name: string = null, private level: number = 0) {
    super();
  }
}
