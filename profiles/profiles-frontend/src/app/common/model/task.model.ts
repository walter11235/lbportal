import { BaseEntity } from "./base-entity.model";

export class Task extends BaseEntity {
  constructor(private description: String = null) {
    super();
  }
}
