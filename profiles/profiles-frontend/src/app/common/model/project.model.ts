import { BaseEntity } from "./base-entity.model";
import { Technology } from "./techproduct/technology.model";
import { Tool } from "./techproduct/tool.model";
import { ProgrammingLanguage } from "./techproduct/programming-language.model";
import { Miscellany } from "./techproduct/miscellany.model";
import { Task } from './task.model';

export class Project extends BaseEntity {
  constructor(public name: string = null,
              public startDate: Date = null,
              public endDate: Date = null,
              public role: string = null,
              public projectSize: string = null,
              public description: string = null,
              public technologies: Technology[] = [],
              public programmingLanguages: ProgrammingLanguage[] = [],
              public tools: Tool[] = [],
              public miscellaneous: Miscellany[] = [],
              public tasks: Task[] = [],
              public projectOrder: number = 0) {
    super();
  }
}
