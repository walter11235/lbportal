
import {TechProduct} from './tech-product.model';

export class ProgrammingLanguage extends TechProduct {
  constructor() {
    super();
    this.type = 'ProgrammingLanguage';
  }
}
