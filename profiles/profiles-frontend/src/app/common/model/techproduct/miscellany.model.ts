import {TechProduct} from './tech-product.model';

export class Miscellany extends TechProduct {
  constructor() {
    super();
    this.type = 'Miscellany';
  }
}
