import {TechProduct} from './tech-product.model';

export class Tool extends TechProduct {
  constructor() {
    super();
    this.type = 'Tool';
  }
}
