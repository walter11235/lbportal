import { BaseEntity } from "../base-entity.model";

export class TechProduct extends BaseEntity {
  constructor(public name: string = null,
              public type: string = null) {
    super();
  }
}
