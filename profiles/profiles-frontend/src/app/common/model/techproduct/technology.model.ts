import {TechProduct} from './tech-product.model';

export class Technology extends TechProduct {
  constructor() {
    super();
    this.type = 'Technology';
  }
}
