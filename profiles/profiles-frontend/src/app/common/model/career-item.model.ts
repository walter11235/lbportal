import { BaseEntity } from "./base-entity.model";

export class CareerItem extends BaseEntity {

  constructor(public startDate: Date = null,
              public endDate: Date = null,
              public description: String = null) {
    super();
  }
}
