import { Technology } from "./techproduct/technology.model";
import { ProgrammingLanguage } from "./techproduct/programming-language.model";
import { Tool } from "./techproduct/tool.model";
import { Miscellany } from "./techproduct/miscellany.model";
import { Project } from "./project.model";
import { Skill } from "./skill.model";
import { CareerItem } from "./career-item.model";
import { BaseEntity } from "./base-entity.model";

export class Employee extends BaseEntity {
  constructor(
    public firstName: string = null,
    public lastName: string = null,
    public company: string = 'LumaBIT GmbH',
    public status: string = null,
    public occupation: string = null,
    public availableDate: Date = null,
    public education: string = null,
    public emailAddress: string = null,
    public technologies: Technology[] = [],
    public programmingLanguages: ProgrammingLanguage[] = [],
    public tools: Tool[] = [],
    public miscellaneous: Miscellany[] = [],
    public careerItems: CareerItem[] = [],
    public projects: Project[] = [],
    public skills: Skill[] = []
  ) {
    super();
  }
}
