import {Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from "rxjs";
import { Person } from "../common/model/person/person.model";
import { AuthService } from "../common/service/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  loggedInUser: Person;

  subscription: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.subscription = this.authService.loggedInPersonSet
      .subscribe((loggedInPerson: Person) => {
        this.loggedInUser = loggedInPerson;
      });
    this.loggedInUser = this.authService.getLoggedInPerson();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
