import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "./common/service/auth.service";
import { DataStorageService } from "./common/service/data-storage.service";
import { AuthResolverService } from "./common/service/resolver/auth-resolver.service";
import { AdminGuard } from "./common/service/guard/admin.guard";
import { AppRoutingModule } from "./app-routing.module";
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { EmployeeItemComponent } from './employees/employee-list/employee-item/employee-item.component';
import { EmployeeService } from "./common/service/employee.service";
import { EmployeeDetailComponent } from './employees/employee-detail/employee-detail.component';
import { EmployeesResolverService } from "./common/service/resolver/employees-resolver.service";
import { CatalogComponent } from './catalog/catalog.component';
import { CatalogListComponent } from './catalog/catalog-list/catalog-list.component';
import { CatalogItemComponent } from './catalog/catalog-list/catalog-item/catalog-item.component';
import { TechProductService } from "./common/service/tech-product.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule
} from "@angular/material";
import { EmployeeEditComponent } from './employees/employee-edit/employee-edit.component';
import { TechProductSelectComponent } from './common/tech-product-select/tech-product-select.component';
import { EditCareerItemComponent } from './employees/employee-edit/edit-career-item/edit-career-item.component';
import { EditSkillItemComponent } from './employees/employee-edit/edit-skill-item/edit-skill-item.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { ProjectItemComponent } from './projects/project-list/project-item/project-item.component';
import { ProjectEditComponent } from './projects/project-edit/project-edit.component';
import { ProjectsResolverService } from "./common/service/resolver/projects-resolver.service";
import { ProjectService } from "./common/service/project.service";
import { EditTaskItemComponent } from './projects/project-edit/edit-task-item/edit-task-item.component';
import { IsHimGuard } from "./common/service/guard/is-him.guard";
import { ErrorDialogComponent } from "./common/dialog/error-dialog/error-dialog.component";
import { EmployeeIndexGuard } from "./common/service/guard/employee-index.guard";
import { ProjectIndexGuard } from "./common/service/guard/project-index.guard";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EmployeesComponent,
    EmployeeListComponent,
    EmployeeItemComponent,
    EmployeeDetailComponent,
    CatalogComponent,
    CatalogListComponent,
    CatalogItemComponent,
    EmployeeEditComponent,
    TechProductSelectComponent,
    EditCareerItemComponent,
    EditSkillItemComponent,
    ProjectsComponent,
    ProjectListComponent,
    ProjectItemComponent,
    ProjectEditComponent,
    EditTaskItemComponent,
    ErrorDialogComponent
  ],
  entryComponents: [
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatOptionModule,
    MatExpansionModule,
    MatDividerModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule
  ],
  providers: [
    AuthService,
    EmployeeService,
    ProjectService,
    DataStorageService,
    TechProductService,
    AuthResolverService,
    EmployeesResolverService,
    ProjectsResolverService,
    AdminGuard,
    IsHimGuard,
    EmployeeIndexGuard,
    ProjectIndexGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
