package de.lumabit.lbportal.profiles.profilesbackend.service.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Technology;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.ProjectRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TechnologyService extends TechProductService {

    @Autowired
    public TechnologyService(TechnologyRepository technologyRepository,
                             EmployeeRepository employeeRepository,
                             ProjectRepository projectRepository) {
        super(technologyRepository, employeeRepository, projectRepository);
    }

    public void refreshEmployees(TechProduct techProduct) {
        Technology technology = (Technology) techProduct;
        technology.getEmployees().forEach( (x) ->
        {
            x.getTechnologies().remove(technology);
            employeeRepository.save(x);
        });
    }

    public void refreshProjects(TechProduct techProduct) {
        Technology technology = (Technology) techProduct;
        technology.getProjects().forEach( (x) ->
        {
            x.getTechnologies().remove(technology);
            projectRepository.save(x);
        });
    }
}
