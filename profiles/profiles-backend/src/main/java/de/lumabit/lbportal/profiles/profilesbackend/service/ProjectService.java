package de.lumabit.lbportal.profiles.profilesbackend.service;

import de.lumabit.lbportal.profiles.profilesbackend.exception.NotFoundException;
import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.model.Project;
import de.lumabit.lbportal.profiles.profilesbackend.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    private ProjectRepository projectRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    public Iterable<Project> getAllByEmployee(Employee employee) {
        return projectRepository.findAllByEmployeeOrderByProjectOrderAsc(employee);
    }

    public Project getById(Long id) {

        return projectRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Project not found"));
    }

    public Project create(Employee employee, Project project) {
        project.setEmployee(employee);
        return projectRepository.save(project);
    }

    public Project update(Employee employee, Project project) {
        project.setEmployee(employee);
        return projectRepository.save(project);
    }

    public void delete(Project project) {
        projectRepository.delete(project);
    }
}
