package de.lumabit.lbportal.profiles.profilesbackend.utility;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Util {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/yyyy");
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /*public String itemString(List<String> strings) {
        String str = strings.toString();
        return str.substring(1, str.length() - 1);
    }*/

    public String itemString(List<? extends TechProduct> techProducts) {
        List<String> strings = techProducts.stream().map(TechProduct::getName).collect(Collectors.toList());
        String str = strings.toString();
        return str.substring(1, str.length() - 1);
    }

    public String formattedDate(LocalDate date, String nullText) {
        if (date == null) {
            return nullText;
        }
        return date.format(formatter);
    }

    public String formattedDate(LocalDate date) {
        return formattedDate(date, "heute");
    }

    public String skillLevelName(int skillLevel) {
        switch (skillLevel) {
            case 0:
                return "Keine Kentnisse";
            case 1:
                return "Grundkentnisse";
            case 2:
                return "Vertiefte Kentnisse";
            case 3:
                return "Spezialwissen";
            default:
                throw new IndexOutOfBoundsException();
        }
    }
}

