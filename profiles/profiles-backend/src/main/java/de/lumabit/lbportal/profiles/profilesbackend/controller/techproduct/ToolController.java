package de.lumabit.lbportal.profiles.profilesbackend.controller.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.ToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tools")
public class ToolController extends TechProductController {

    @Autowired
    public ToolController(ToolService toolService) {
        super(toolService);
    }
}
