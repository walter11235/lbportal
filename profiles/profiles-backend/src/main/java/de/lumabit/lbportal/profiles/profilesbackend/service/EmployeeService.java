package de.lumabit.lbportal.profiles.profilesbackend.service;

import de.lumabit.lbportal.profiles.profilesbackend.exception.NotFoundException;
import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;
    //private PersonRepository personRepository;
    //Constructor based dependency injection
    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
        //this.personRepository = personRepository;
    }

    public Iterable<Employee> list(){
        return employeeRepository.findAll();
    }

    public Employee getById(Long id){

        return employeeRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Employee not found"));
    }

    public Employee update(Long id, Employee _employee) {
        return employeeRepository.save(_employee);
    }

    //Expression-Based Access Control
    @PreAuthorize("hasAuthority('admin') or #employee.emailAddress == authentication.name")
    public void delete(Long id, Employee employee) {
         employeeRepository.delete(employee);
    }

    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Iterable<Employee> refresh() {
        /*Iterable<Person> persons = personRepository.findAll();
        persons.forEach(person -> {
            if(!employeeRepository.existsEmployeeByEmailAddress(person.getEmailAddress())) {
                Employee employee = new Employee();
                employee.setFirstName(person.getFirstName());
                employee.setLastName(person.getLastName());
                employee.setEmailAddress(person.getEmailAddress());
                employee.setCareerItems(new ArrayList<>());
                employee.setMiscellaneous(new ArrayList<>());
                employee.setProgrammingLanguages(new ArrayList<>());
                employee.setProjects(new ArrayList<>());
                employee.setTechnologies(new ArrayList<>());
                employee.setTools(new ArrayList<>());
                employee.setSkills(new ArrayList<>());
                employeeRepository.save(employee);
            }
        });*/
        return employeeRepository.findAll();
    }
}
