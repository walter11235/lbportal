package de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
//https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.single-repository-behaviour
@NoRepositoryBean
public interface TechProductRepository<T extends TechProduct> extends CrudRepository<T, Long> {
    Iterable<T> findAllByOrderByNameAsc();
}
