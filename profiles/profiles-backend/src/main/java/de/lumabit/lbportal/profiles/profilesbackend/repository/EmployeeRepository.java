package de.lumabit.lbportal.profiles.profilesbackend.repository;

import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Employee findByEmailAddress(String emailAddress);
    Boolean  existsEmployeeByEmailAddress(String emailAddress);
}
