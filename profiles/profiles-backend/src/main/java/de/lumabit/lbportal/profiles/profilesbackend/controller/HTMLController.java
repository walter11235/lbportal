package de.lumabit.lbportal.profiles.profilesbackend.controller;

import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import de.lumabit.lbportal.profiles.profilesbackend.utility.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;


@RestController
@RequestMapping("/html")
@Configuration
public class HTMLController {
    //private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final EmployeeRepository employeeRepository;
    private final TemplateEngine templateEngine;

    private final Context context;

    @Autowired
    public HTMLController(TemplateEngine templateEngine, EmployeeRepository employeeRepository) {
        this.templateEngine = templateEngine;
        this.employeeRepository = employeeRepository;
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");

        templateEngine.setTemplateResolver(templateResolver);

        context = new Context();
    }

    @RequestMapping("/{id}")
    public String getHTML(@PathVariable("id") Long id) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        context.setVariable("employee", employee);
        context.setVariable("util", new Util());
// Get the plain HTML with the resolved ${name} variable!
        return templateEngine.process("templates/template", context);

    }

    public String getHTML(Long id, String temp) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        context.setVariable("employee", employee);
        context.setVariable("util", new Util());
// Get the plain HTML with the resolved ${name} variable!
        return templateEngine.process("templates/"+temp, context);

    }

}
