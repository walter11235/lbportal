package de.lumabit.lbportal.profiles.profilesbackend.controller.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.MiscellanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/miscellaneous")
public class MiscellanyController extends TechProductController {

    @Autowired
    public MiscellanyController(MiscellanyService miscellanyService) {
        super(miscellanyService);
    }
}
