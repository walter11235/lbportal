package de.lumabit.lbportal.profiles.profilesbackend.controller;

import com.itextpdf.text.DocumentException;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.Collections;

@RestController
@RequestMapping("/pdf")
@Component
@Slf4j
public class PDFController {
    private final HTMLController htmlController;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public PDFController(HTMLController htmlController, EmployeeRepository employeeRepository) {
        this.htmlController = htmlController;
        this.employeeRepository = employeeRepository;
    }

    @RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public FileSystemResource getPDF(@PathVariable("id") Long id, @RequestParam("temp") String temp) throws IOException, DocumentException {
        String html = htmlController.getHTML(id, temp);
        File file = new File("Mitarbeiterprofil " + employeeRepository.findById(id).get().getTitle() + ".pdf");
        OutputStream outputStream = new FileOutputStream(file);
        ITextRenderer renderer = new ITextRenderer();
        ITextFontResolver fontResolver = renderer.getFontResolver();

        ClassLoader loader = getClass().getClassLoader();
        URI uri = doGetURI(loader, "static/fonts");
        URI imageUri = doGetURI(loader, "static/images");
        System.out.println(uri);
        Path fontPath = null;
        Path imagePath = null;
        if (uri.getScheme().equals("jar")) {
            FileSystem fileSystem = doCreateFileSystem(uri);
            fontPath = fileSystem.getPath("/BOOT-INF/classes/static/fonts/");
            imagePath = fileSystem.getPath("/BOOT-INF/classes/static/images/");
        } else {
            fontPath = Paths.get(uri);
            imagePath = Paths.get(imageUri);
        }

        //String fontPath = getClass().getResource("/static/fonts").getPath().substring(1);
        Files.walk(fontPath)
                .filter(Files::isRegularFile)
                .forEach(fontFile -> {
                    try {
                        fontResolver.addFont(fontFile.toString(), true);
                    } catch (DocumentException e) {
                        log.error("Error while adding font", e);
                    } catch (IOException e) {
                        log.error("Error while file walk", e.getMessage());
                    }
                });
        //https://github.com/tuhrig/Flying_Saucer_PDF_Generation/blob/master/src/test/java/FlyingSaucerTest.java
        /*String baseUrl = FileSystems
                .getDefault()
                .getPath("target", "classes", "static", "images")
                .toUri()
                .toURL()
                .toString();*/
        String baseUrl = imagePath.toUri().toURL().toString();
        renderer.setDocumentFromString(html, baseUrl);
        renderer.layout();
        renderer.createPDF(outputStream);
        outputStream.close();
        return new FileSystemResource(file);
    }

    /**
     * Creates the URI of default directory
     *
     * @return URI of default directory
     */
    private URI doGetURI(ClassLoader _loader, String _name) {

        try {
            return _loader.getResource(_name).toURI();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }

    }

    /**
     * Creates new file system for current jar using URI
     *
     * FileSystem Reference:
     *
     * https://docs.oracle.com/javase/8/docs/api/java/nio/file/FileSystems.html
     *
     * @param _uri
     *            the URI of the default directory
     *
     * @return FileSystem of default directory
     */
    private FileSystem doCreateFileSystem(URI _uri) {

        try {
            return FileSystems.newFileSystem(_uri, Collections.<String, Object>emptyMap());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } catch (FileSystemAlreadyExistsException e) {
            return FileSystems.getFileSystem(_uri);
        }
    }
}
