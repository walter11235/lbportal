package de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct;


import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Technology;

public interface TechnologyRepository extends TechProductRepository<Technology> {
}
