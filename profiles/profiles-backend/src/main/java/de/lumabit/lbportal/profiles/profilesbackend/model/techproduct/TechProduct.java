package de.lumabit.lbportal.profiles.profilesbackend.model.techproduct;

import com.fasterxml.jackson.annotation.*;
import de.lumabit.lbportal.profiles.profilesbackend.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//to resolve cannot instantiate TechProduct issue
// when we try to map sub class on abstract class
@EqualsAndHashCode(callSuper = true)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY, //field must be present in the POJO
        property = "type"
        )
@JsonSubTypes({
        @JsonSubTypes.Type(value = ProgrammingLanguage.class, name = "ProgrammingLanguage"),
        @JsonSubTypes.Type(value = Technology.class, name = "Technology"),
        @JsonSubTypes.Type(value = Tool.class, name = "Tool"),
        @JsonSubTypes.Type(value = Miscellany.class, name = "Miscellany")
})
@Data
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public abstract class TechProduct extends BaseEntity {

    public TechProduct(String name) {
        this.name = name;
    }

    //Here we have to initialize this field manually.
    //Here is the simple workaround to initialize it automatically
    @JsonProperty
    @Transient
    private String type = this.getClass().getSimpleName();

    private String name;
}
