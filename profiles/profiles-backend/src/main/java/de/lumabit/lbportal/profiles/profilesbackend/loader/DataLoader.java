package de.lumabit.lbportal.profiles.profilesbackend.loader;

import de.lumabit.lbportal.profiles.profilesbackend.model.*;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Miscellany;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.ProgrammingLanguage;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Technology;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Tool;
import de.lumabit.lbportal.profiles.profilesbackend.service.EmployeeService;
import de.lumabit.lbportal.profiles.profilesbackend.service.ProjectService;
import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.MiscellanyService;
import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.ProgrammingLanguageService;
import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.TechnologyService;
import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.ToolService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    private final EmployeeService employeeService;
    private final ProjectService projectService;
    private final MiscellanyService miscellanyService;
    private final ProgrammingLanguageService programmingLanguageService;
    private final ToolService toolService;
    private final TechnologyService technologyService;


    public DataLoader(EmployeeService employeeService,
                      ProjectService projectService,
                      MiscellanyService miscellanyService,
                      ProgrammingLanguageService programmingLanguageService,
                      ToolService toolService,
                      TechnologyService technologyService) {
        this.employeeService = employeeService;
        this.projectService = projectService;
        this.miscellanyService = miscellanyService;
        this.programmingLanguageService = programmingLanguageService;
        this.toolService = toolService;
        this.technologyService = technologyService;
    }

    @Override
    public void run(String... args) throws Exception {
        Miscellany miscellany1 = Miscellany.builder().name("miscellany1").build();
        Miscellany miscellany2 = Miscellany.builder().name("miscellany2").build();
        Miscellany miscellany3 = Miscellany.builder().name("miscellany3").build();
        miscellanyService.create(miscellany1);
        miscellanyService.create(miscellany2);
        miscellanyService.create(miscellany3);

        ProgrammingLanguage programmingLanguage1 =
                ProgrammingLanguage.builder().name("programmingLanguage1").build();
        ProgrammingLanguage programmingLanguage2 =
                ProgrammingLanguage.builder().name("programmingLanguage2").build();
        ProgrammingLanguage programmingLanguage3 =
                ProgrammingLanguage.builder().name("programmingLanguage3").build();
        programmingLanguageService.create(programmingLanguage1);
        programmingLanguageService.create(programmingLanguage2);
        programmingLanguageService.create(programmingLanguage3);

        Technology technology1 = Technology.builder().name("technology1").build();
        Technology technology2 = Technology.builder().name("technology2").build();
        Technology technology3 = Technology.builder().name("technology3").build();
        technologyService.create(technology1);
        technologyService.create(technology2);
        technologyService.create(technology3);

        Tool tool1 = Tool.builder().name("tool1").build();
        Tool tool2 = Tool.builder().name("tool2").build();
        Tool tool3 = Tool.builder().name("tool3").build();
        toolService.create(tool1);
        toolService.create(tool2);
        toolService.create(tool3);

        Task task1 = Task.builder().description("task1").build();
        Task task2 = Task.builder().description("task2").build();
        Task task3 = Task.builder().description("task3").build();
        Task task4 = Task.builder().description("task4").build();
        Task task5 = Task.builder().description("task5").build();
        Task task6 = Task.builder().description("task6").build();
        Task task7 = Task.builder().description("task7").build();
        Task task8 = Task.builder().description("task8").build();

        Project project1 = Project.builder().name("project1")
                .description("descripition_project1")
                .projectOrder(1L)
                .projectSize("size_project1")
                .role("role_project1")
                .startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .miscellaneous(new ArrayList<>(Arrays.asList(miscellany1,miscellany2,miscellany3)))
                .programmingLanguages(new ArrayList<>(Arrays.asList(programmingLanguage1,
                        programmingLanguage2,programmingLanguage3)))
                .tools(new ArrayList<>(Arrays.asList(tool1,tool2,tool3)))
                .technologies(new ArrayList<>(Arrays.asList(technology1,technology2,technology3)))
                .tasks(new ArrayList<>(Arrays.asList(task1,task2)))
                .build();

        Project project2 = Project.builder().name("project2")
                .description("descripition_project2")
                .projectOrder(2L)
                .projectSize("size_project2")
                .role("role_project2")
                .startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .miscellaneous(new ArrayList<>(Arrays.asList(miscellany1,miscellany2,miscellany3)))
                .programmingLanguages(new ArrayList<>(Arrays.asList(programmingLanguage1,
                        programmingLanguage2,programmingLanguage3)))
                .tools(new ArrayList<>(Arrays.asList(tool1,tool2,tool3)))
                .technologies(new ArrayList<>(Arrays.asList(technology1,technology2,technology3)))
                .tasks(new ArrayList<>(Arrays.asList(task3,task4)))
                .build();

        Project project3 = Project.builder().name("project3")
                .description("descripition_project3")
                .projectOrder(3L)
                .projectSize("size_project3")
                .role("role_project3")
                .startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .miscellaneous(new ArrayList<>(Arrays.asList(miscellany1,miscellany2,miscellany3)))
                .programmingLanguages(new ArrayList<>(Arrays.asList(programmingLanguage1,
                        programmingLanguage2,programmingLanguage3)))
                .tools(new ArrayList<>(Arrays.asList(tool1,tool2,tool3)))
                .technologies(new ArrayList<>(Arrays.asList(technology1,technology2,technology3)))
                .tasks(new ArrayList<>(Arrays.asList(task5,task6)))
                .build();

        Project project4 = Project.builder().name("project4")
                .description("descripition_project4")
                .projectOrder(4L)
                .projectSize("size_project4")
                .role("role_project4")
                .startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .miscellaneous(new ArrayList<>(Arrays.asList(miscellany1,miscellany2,miscellany3)))
                .programmingLanguages(new ArrayList<>(Arrays.asList(programmingLanguage1,
                        programmingLanguage2,programmingLanguage3)))
                .tools(new ArrayList<>(Arrays.asList(tool1,tool2,tool3)))
                .technologies(new ArrayList<>(Arrays.asList(technology1,technology2,technology3)))
                .tasks(new ArrayList<>(Arrays.asList(task7,task8)))
                .build();

        CareerItem careerItem1 = CareerItem.builder().startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .description("description_careerItem1")
                .build();
        CareerItem careerItem2 = CareerItem.builder().startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .description("description_careerItem2")
                .build();
        CareerItem careerItem3 = CareerItem.builder().startDate(LocalDate.of(2002,2,2))
                .endDate(LocalDate.of(2002,2,3))
                .description("description_careerItem3")
                .build();

        Skill skill1 = Skill.builder().level(1L).name("skill1").build();
        Skill skill2 = Skill.builder().level(2L).name("skill2").build();
        Skill skill3 = Skill.builder().level(3L).name("skill3").build();


        Employee employee1 = Employee.builder().firstName("fname_employee1")
                .lastName("lname_employee1")
                .availableDate(LocalDate.of(2002,2,2))
                .company("company_employee1")
                .education("education_employee1")
                .emailAddress("vijayamohan@lumabit.de")
                .googleId("googleId_employee1")
                .occupation("occupation_employee1")
                .status("status_employee1")
                .miscellaneous(new ArrayList<>(Arrays.asList(miscellany1, miscellany2, miscellany3)))
                .programmingLanguages(new ArrayList<>(Arrays.asList(programmingLanguage1,
                         programmingLanguage2, programmingLanguage3)))
                .tools(new ArrayList<>(Arrays.asList(tool1, tool2, tool3)))
                .technologies(new ArrayList<>(Arrays.asList(technology1, technology2, technology3)))
                .careerItems(new ArrayList<>(Arrays.asList(careerItem1, careerItem2)))
                .skills(new ArrayList<>(Arrays.asList(skill1, skill2)))
                .build();

        Employee employee2 = Employee.builder().firstName("fname_employee2")
                .lastName("lname_employee2")
                .availableDate(LocalDate.of(2002,2,2))
                .company("company_employee2")
                .education("education_employee2")
                .emailAddress("vijayamohan.h@gmail.com")
                .googleId("googleId_employee2")
                .occupation("occupation_employee2")
                .status("status_employee2")
                .miscellaneous(new ArrayList<>(Arrays.asList(miscellany1, miscellany2, miscellany3)))
                .programmingLanguages(new ArrayList<>(Arrays.asList(programmingLanguage1,
                        programmingLanguage2, programmingLanguage3)))
                .tools(new ArrayList<>(Arrays.asList(tool1, tool2, tool3)))
                .technologies(new ArrayList<>(Arrays.asList(technology1, technology2, technology3)))
                .careerItems(new ArrayList<>(Arrays.asList(careerItem3)))
                .skills(new ArrayList<>(Arrays.asList(skill3)))
                .build();

        employeeService.create(employee1);
        projectService.create(employee1, project1);
        projectService.create(employee1, project2);


        employeeService.create(employee2);
        projectService.create(employee2, project3);
        projectService.create(employee2, project4);


    }
}
