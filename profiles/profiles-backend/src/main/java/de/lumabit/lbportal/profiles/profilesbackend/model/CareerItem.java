package de.lumabit.lbportal.profiles.profilesbackend.model;

import lombok.*;

import javax.persistence.Entity;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CareerItem extends BaseEntity {

    private LocalDate startDate;
    private LocalDate endDate;
    private String description;
}
