package de.lumabit.lbportal.profiles.profilesbackend.model.techproduct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.model.Project;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Technology extends TechProduct {

    @Builder
    public Technology(String name) {
        super(name);
    }

    @JsonIgnore
    @ManyToMany(mappedBy = "technologies")
    private List<Employee> employees;

    @JsonIgnore
    @ManyToMany(mappedBy = "technologies")
    private List<Project> projects;
}
