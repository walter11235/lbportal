package de.lumabit.lbportal.profiles.profilesbackend.controller.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.ProgrammingLanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/programminglanguages")
public class ProgrammingLanguageController extends TechProductController {

    @Autowired
    public ProgrammingLanguageController(ProgrammingLanguageService programmingLanguageService) {
         super(programmingLanguageService);
    }

}
