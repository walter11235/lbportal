package de.lumabit.lbportal.profiles.profilesbackend.controller.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.TechProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public abstract class TechProductController {

    protected TechProductService techProductService;

    @Autowired
    public TechProductController(TechProductService techProductService) {
        this.techProductService = techProductService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Iterable<? extends TechProduct> getAll() {
        return techProductService.findAll();
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public TechProduct update(@PathVariable Long id, @RequestBody TechProduct techProduct) {
        return techProductService.update(id, techProduct);
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public TechProduct create(@RequestBody TechProduct techProduct) {
        return techProductService.create(techProduct);
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) throws Throwable {
        TechProduct techProduct = techProductService.getById(id);
        techProductService.delete(techProduct);
    }
}
