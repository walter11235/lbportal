package de.lumabit.lbportal.profiles.profilesbackend.service.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.exception.NotFoundException;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.ProjectRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct.TechProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public abstract class TechProductService {

    TechProductRepository techProductRepository;
    EmployeeRepository employeeRepository;
    ProjectRepository projectRepository;

    @Autowired
    public TechProductService(TechProductRepository techProductRepository,
                              EmployeeRepository employeeRepository,
                              ProjectRepository projectRepository) {
        this.techProductRepository = techProductRepository;
        this.employeeRepository = employeeRepository;
        this.projectRepository = projectRepository;
    }

    protected abstract void refreshEmployees(TechProduct techProduct);

    protected abstract void refreshProjects(TechProduct techProduct);

    public Iterable<? extends TechProduct> findAll() {
        return techProductRepository.findAllByOrderByNameAsc();
    }

    public TechProduct update(Long id, TechProduct techProduct) {
        return (TechProduct) techProductRepository.save(techProduct);
    }

    public TechProduct create(TechProduct techProduct) {
        return (TechProduct) techProductRepository.save(techProduct);
    }

    public void delete(TechProduct techProduct) {
        refreshEmployees(techProduct);
        refreshProjects(techProduct);
        techProductRepository.delete(techProduct);
    }

    public TechProduct getById(Long id) throws Throwable {
         return (TechProduct) techProductRepository.findById(id).orElseThrow(
                 () -> new NotFoundException("TechProduct not found"));
    }
}
