package de.lumabit.lbportal.profiles.profilesbackend.exception;


public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}
