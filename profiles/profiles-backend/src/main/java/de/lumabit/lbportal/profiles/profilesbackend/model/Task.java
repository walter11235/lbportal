package de.lumabit.lbportal.profiles.profilesbackend.model;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task extends BaseEntity {

    private String description;
}
