package de.lumabit.lbportal.profiles.profilesbackend.model.techproduct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.model.Project;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tool extends TechProduct {

    @Builder
    public Tool(String name) {
        super(name);
    }

    @JsonIgnore
    @ManyToMany(mappedBy = "tools")
    private List<Employee> employees;

    @JsonIgnore
    @ManyToMany(mappedBy = "tools")
    private List<Project> projects;
}
