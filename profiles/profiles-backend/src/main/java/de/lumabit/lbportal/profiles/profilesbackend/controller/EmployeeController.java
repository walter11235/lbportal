package de.lumabit.lbportal.profiles.profilesbackend.controller;

import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeService employeeService;
    //Constructor based dependency injection
    @Autowired
    public EmployeeController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public Iterable<Employee> list(){
        return this.employeeService.list();
    }

    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public Principal getPrincipal(Principal principal) {
        //System.out.println(principal.getName());
        return principal;
    }

    //Expression-Based Access Control
    @PreAuthorize("hasAuthority('admin') or #employee.googleId == authentication.name")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Employee create(@RequestBody Employee employee) {
        return employeeService.create(employee);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Employee getById(@PathVariable Long id) {
        return employeeService.getById(id);
    }
    //Expression-Based Access Control
    //@Secured("admin")
    @PreAuthorize("hasAuthority('admin') or #employee.googleId == authentication.name")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Employee update(@PathVariable Long id, @RequestBody  Employee employee) {
        return employeeService.update(id, employee);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        Employee employee = employeeService.getById(id);
        employeeService.delete(id, employee);
    }

    @RequestMapping(value = "/refresh",method = RequestMethod.GET)
    public Iterable<Employee> refreshEmployees(){
        return this.employeeService.refresh();
    }
}
