package de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.ProgrammingLanguage;

public interface ProgrammingLanguageRepository extends TechProductRepository<ProgrammingLanguage> {
}
