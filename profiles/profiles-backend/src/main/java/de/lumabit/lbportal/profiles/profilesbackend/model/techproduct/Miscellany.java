package de.lumabit.lbportal.profiles.profilesbackend.model.techproduct;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.model.Project;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Miscellany extends TechProduct {

    @Builder
    public Miscellany(String name) {
        super(name);
    }

    @JsonIgnore
    @ManyToMany(mappedBy = "miscellaneous")
    private List<Employee> employees;

    @JsonIgnore
    @ManyToMany(mappedBy = "miscellaneous")
    private List<Project> projects;
}
