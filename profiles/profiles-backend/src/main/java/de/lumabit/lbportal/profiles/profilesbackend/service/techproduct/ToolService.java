package de.lumabit.lbportal.profiles.profilesbackend.service.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Tool;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.ProjectRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct.ToolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ToolService extends TechProductService {

    @Autowired
    public ToolService(ToolRepository toolRepository,
                       EmployeeRepository employeeRepository,
                       ProjectRepository projectRepository) {
        super(toolRepository, employeeRepository, projectRepository);
    }

    public void refreshEmployees(TechProduct techProduct) {
        Tool tool = (Tool) techProduct;
        tool.getEmployees().forEach((x) ->
        {
            x.getTools().remove(tool);
            employeeRepository.save(x);
        });
    }

    public void refreshProjects(TechProduct techProduct) {
        Tool tool = (Tool) techProduct;
        tool.getProjects().forEach((x) ->
        {
            x.getTools().remove(tool);
            projectRepository.save(x);
        });
    }
}
