package de.lumabit.lbportal.profiles.profilesbackend.controller;

import de.lumabit.lbportal.profiles.profilesbackend.model.Project;
import de.lumabit.lbportal.profiles.profilesbackend.service.EmployeeService;
import de.lumabit.lbportal.profiles.profilesbackend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/projects")
public class ProjectController {

    private ProjectService projectService;
    private EmployeeService employeeService;

    @Autowired
    public ProjectController (ProjectService projectService, EmployeeService employeeService) {
        this.projectService = projectService;
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Iterable<Project> getByEmployeeId(@RequestParam("empid") Long id) {
        return projectService.getAllByEmployee(employeeService.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Project getById(@PathVariable("id") Long id) {
        return projectService.getById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("id") Long id) {
        Project project = projectService.getById(id);
        projectService.delete(project);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Project create(@RequestParam("empid") Long id, @RequestBody Project project) {
        return projectService.create(employeeService.getById(id),project);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public Project update(@RequestParam("empid") Long id, @RequestBody Project project) {
        return projectService.update(employeeService.getById(id),project);
    }
}