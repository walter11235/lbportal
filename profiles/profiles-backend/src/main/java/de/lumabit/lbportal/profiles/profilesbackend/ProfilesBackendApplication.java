package de.lumabit.lbportal.profiles.profilesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ProfilesBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProfilesBackendApplication.class, args);
    }

}
