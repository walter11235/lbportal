package de.lumabit.lbportal.profiles.profilesbackend.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Miscellany;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.ProgrammingLanguage;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Technology;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Tool;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee extends BaseEntity {

    //no arg constructor not required when we use lombok
    //public Employee() {}

    private String firstName;

    private String lastName;

    private String company;

    private String status;

    private String occupation;

    private LocalDate availableDate;

    private String education;

    private String googleId;

    @NotNull
    @Column(unique = true)
    private String emailAddress;

    @ManyToMany
    @JoinTable(name="employee_technology", joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "technology_id"))
    @OrderColumn
    private List<Technology> technologies;

    @ManyToMany
    @JoinTable(name="employee_programminglanguage", joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "programminglanguage_id"))
    @OrderColumn
    private List<ProgrammingLanguage> programmingLanguages;

    @ManyToMany
    @JoinTable(name="employee_tool", joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "tool_id"))
    @OrderColumn
    private List<Tool> tools;

    @ManyToMany
    @JoinTable(name="employee_miscellany", joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "miscellany_id"))
    @OrderColumn
    private List<Miscellany> miscellaneous;

    @OneToMany(cascade = CascadeType.ALL)
    @OrderBy("endDate desc NULLS first, startDate desc, description")
    private List<CareerItem> careerItems;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employee")
    @OrderBy("projectOrder asc")
    @JsonManagedReference
    private List<Project> projects;


    @OneToMany(cascade = CascadeType.ALL)
    @OrderColumn
    private List<Skill> skills;

    public String getTitle() {
        return firstName + " " + lastName;
    }

}
