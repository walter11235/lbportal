package de.lumabit.lbportal.profiles.profilesbackend.service.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Miscellany;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.ProjectRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct.MiscellanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MiscellanyService extends TechProductService {

    @Autowired
    public MiscellanyService(MiscellanyRepository miscellanyRepository,
                             EmployeeRepository employeeRepository,
                             ProjectRepository projectRepository) {
        super(miscellanyRepository, employeeRepository, projectRepository);
    }

    public void refreshEmployees(TechProduct techProduct) {
        Miscellany miscellany = (Miscellany) techProduct;
        miscellany.getEmployees().forEach( (x) ->
        {
            x.getMiscellaneous().remove(miscellany);
            employeeRepository.save(x);
        });
    }

    public void refreshProjects(TechProduct techProduct) {
        Miscellany miscellany = (Miscellany) techProduct;
        miscellany.getProjects().forEach( (x) ->
        {
            x.getMiscellaneous().remove(miscellany);
            projectRepository.save(x);

        });
    }
}
