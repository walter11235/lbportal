package de.lumabit.lbportal.profiles.profilesbackend.service.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.ProgrammingLanguage;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.TechProduct;
import de.lumabit.lbportal.profiles.profilesbackend.repository.EmployeeRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.ProjectRepository;
import de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct.ProgrammingLanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProgrammingLanguageService extends TechProductService {

   @Autowired
    public ProgrammingLanguageService(ProgrammingLanguageRepository programmingLanguageRepository,
                                      EmployeeRepository employeeRepository,
                                      ProjectRepository projectRepository) {
        super(programmingLanguageRepository, employeeRepository, projectRepository);
    }

    public void refreshEmployees(TechProduct techProduct) {
        ProgrammingLanguage programmingLanguage = (ProgrammingLanguage) techProduct;
        programmingLanguage.getEmployees().forEach( (x) ->
        {
            x.getProgrammingLanguages().remove(programmingLanguage);
            employeeRepository.save(x);
        });
    }

    public void refreshProjects(TechProduct techProduct) {
        ProgrammingLanguage programmingLanguage = (ProgrammingLanguage) techProduct;
        programmingLanguage.getProjects().forEach( (x) ->
        {
            x.getProgrammingLanguages().remove(programmingLanguage);
            projectRepository.save(x);
        });
    }
}
