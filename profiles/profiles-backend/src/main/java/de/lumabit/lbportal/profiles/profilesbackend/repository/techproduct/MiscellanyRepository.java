package de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Miscellany;

public interface MiscellanyRepository extends TechProductRepository<Miscellany> {
}
