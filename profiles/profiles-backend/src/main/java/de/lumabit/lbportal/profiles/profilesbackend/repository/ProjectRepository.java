package de.lumabit.lbportal.profiles.profilesbackend.repository;

import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.model.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long> {

    Iterable<Project> findAllByEmployeeOrderByProjectOrderAsc(Employee employee);
}
