package de.lumabit.lbportal.profiles.profilesbackend.repository.techproduct;


import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Tool;

public interface ToolRepository extends TechProductRepository<Tool> {
}
