package de.lumabit.lbportal.profiles.profilesbackend.controller.techproduct;

import de.lumabit.lbportal.profiles.profilesbackend.service.techproduct.TechnologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/technologies")
public class TechnologyController extends TechProductController {

    @Autowired
    public TechnologyController(TechnologyService technologyService) {
        super(technologyService);
    }
}
