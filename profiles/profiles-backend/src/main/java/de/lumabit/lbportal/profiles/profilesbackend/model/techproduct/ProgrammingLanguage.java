package de.lumabit.lbportal.profiles.profilesbackend.model.techproduct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.lumabit.lbportal.profiles.profilesbackend.model.Employee;
import de.lumabit.lbportal.profiles.profilesbackend.model.Project;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProgrammingLanguage extends TechProduct {

    @Builder
    public ProgrammingLanguage(String name) {
        super(name);
    }

    @JsonIgnore
    @ManyToMany(mappedBy = "programmingLanguages")
    private List<Employee> employees;

    @JsonIgnore
    @ManyToMany(mappedBy = "programmingLanguages")
    private List<Project> projects;
}
