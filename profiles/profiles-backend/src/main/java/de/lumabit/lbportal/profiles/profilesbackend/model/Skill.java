package de.lumabit.lbportal.profiles.profilesbackend.model;

import lombok.*;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Skill extends BaseEntity {

    private String name;
    private Long level;
}
