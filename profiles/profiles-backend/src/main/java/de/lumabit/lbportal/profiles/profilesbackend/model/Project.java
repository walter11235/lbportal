package de.lumabit.lbportal.profiles.profilesbackend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Miscellany;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.ProgrammingLanguage;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Technology;
import de.lumabit.lbportal.profiles.profilesbackend.model.techproduct.Tool;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Project extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "employee_id")
    @JsonBackReference
    private Employee employee;

    private String name;
    private LocalDate startDate;
    private LocalDate endDate;
    private String role;
    private String projectSize;

    @Column(length = 4096)
    private String description;

    @ManyToMany
    @JoinTable(name="project_technology", joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "technology_id"))
    @OrderColumn
    private List<Technology> technologies;

    @ManyToMany
    @JoinTable(name="project_programminglanguage", joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "programminglanguage_id"))
    @OrderColumn
    private List<ProgrammingLanguage> programmingLanguages;

    @ManyToMany
    @JoinTable(name="project_tool", joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "tool_id"))
    @OrderColumn
    private List<Tool> tools;

    @ManyToMany
    @JoinTable(name="project_miscellany", joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "miscellany_id"))
    @OrderColumn
    private List<Miscellany> miscellaneous;

    @OneToMany(cascade = CascadeType.ALL)
    @OrderColumn
    private List<Task> tasks;

    @Column(name = "project_order", nullable = false, columnDefinition = "bigint default 0")
    private Long projectOrder;
}
