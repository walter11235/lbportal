package de.lumabit.lbportal.usermanagementfrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UsermanagementFrontendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsermanagementFrontendApplication.class, args);
    }

}
