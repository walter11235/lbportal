import { Component, OnInit } from '@angular/core';
import { PersonService } from "../../common/service/person.service";
import { Person } from "../../common/model/person.model";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { DataStorageService } from "../../common/service/data-storage.service";
import { AuthService } from "../../common/service/auth.service";
import { MatDialog } from "@angular/material";
import { ErrorMessage } from "../../common/dialog/error-dialog/error-message.interface";
import {ErrorDialogComponent} from "../../common/dialog/error-dialog/error-dialog.component";

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit {

  person: Person;
  id:  number;

  constructor(private personService: PersonService,
              private dataStorageService: DataStorageService,
              private route: ActivatedRoute,
              public authService: AuthService,
              private dialog: MatDialog,
              private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.person = this.personService.getPersonByIndex(this.id);
        }
      );
  }

  onEditPerson() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeletePerson() {
    this.dataStorageService.deletePerson(this.id, this.person).subscribe(response =>
      this.router.navigate(['../'], {relativeTo: this.route}),
      errorMessage => {
        this.openErrorDialog(errorMessage);
      }
    );
  }

  isHim() {
    return this.authService.isHim(this.person);
  }

  openErrorDialog(errorMessage: ErrorMessage): void {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMessage,
      width: '250px',
      height: '100px'
    })
  }

}
