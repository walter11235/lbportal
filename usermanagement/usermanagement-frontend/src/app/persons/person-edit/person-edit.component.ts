import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { Role } from "../../common/model/role.model";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { PersonService } from "../../common/service/person.service";
import { RoleService } from "../../common/service/role.service";
import { Person } from "../../common/model/person.model";
import { DataStorageService } from "../../common/service/data-storage.service";
import { Observable } from "rxjs";
import {ErrorMessage} from "../../common/dialog/error-dialog/error-message.interface";
import {ErrorDialogComponent} from "../../common/dialog/error-dialog/error-dialog.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {

  //@ViewChild('f', {static: true}) personForm: NgForm;

  id: number;
  person: Person = null;
  editMode = false;
  defaultRoles: Role[] = [];

  constructor(private route: ActivatedRoute,
              private personService: PersonService,
              private roleService: RoleService,
              private dataStorageService: DataStorageService,
              private dialog: MatDialog,
              private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.editMode = params['id'] != null;
          this.defaultRoles = this.roleService.getRoles();
          if(this.editMode) {
            this.person = this.personService.getPersonByIndex(this.id);
          } else {
            this.person = new Person();
          }
        });
  }

  onSubmit() {
    /*this.person.firstName = this.personForm.value.firstName;
    this.person.lastName = this.personForm.value.lastName;
    this.person.address = this.personForm.value.address;
    this.person.birthDate = this.personForm.value.birthDate;
    this.person.emailAddress = this.personForm.value.emailAddress;
    this.person.roles = this.personForm.value.roles;*/
    let observable : Observable<any>;
    if(this.editMode) {
      observable = this.dataStorageService.updatePerson(this.id, this.person);
    } else {
      observable = this.dataStorageService.addPerson(this.person);
    }
    observable.subscribe(
      response => {this.onCancel();},
      errorMessage => {console.log(errorMessage); this.openErrorDialog(errorMessage);}
      )
  }

  equals(objOne, objTwo) {
    if ((objOne !== null && objTwo !== null) &&
      (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined')) {
      return objOne.id === objTwo.id;
    }
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  openErrorDialog(errorMessage: ErrorMessage): void {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMessage,
      width: '500px',
      height: '250px'
    })
  }

}
