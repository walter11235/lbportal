import { Component, OnDestroy, OnInit } from '@angular/core';
import { Person } from "../../common/model/person.model";
import { PersonService } from "../../common/service/person.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import {AuthService} from "../../common/service/auth.service";

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit, OnDestroy {

  persons: Person[] = [];

  subscription: Subscription;

  constructor(private personService: PersonService,
              private route: ActivatedRoute,
              public authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.subscription = this.personService.personsChanged
      .subscribe((persons: Person[]) => {
        this.persons = persons;
      });
    this.persons = this.personService.getPersons();
  }

  onNew() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
