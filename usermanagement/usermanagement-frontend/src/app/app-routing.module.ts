import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PersonsComponent } from "./persons/persons.component";
import { PersonDetailComponent } from "./persons/person-detail/person-detail.component";
import { RolesComponent } from "./roles/roles.component";
import { PersonEditComponent } from "./persons/person-edit/person-edit.component";
import { PersonStartComponent } from "./persons/person-start/person-start.component";
import { RoleEditComponent } from "./roles/role-edit/role-edit.component";
import { PersonsResolverService } from "./common/service/resolver/persons-resolver.service";
import { RolesResolverService } from "./common/service/resolver/roles-resolver.service";
import { AuthResolverService } from "./common/service/resolver/auth-resolver.service";
import { AdminGuard } from "./common/service/guard/admin.guard";
import { IsHimGuard } from "./common/service/guard/is-him.guard";
import { PersonIndexGuard } from "./common/service/guard/person-index.guard";
import { RoleIndexGuard } from "./common/service/guard/role-index.guard";

const appRoutes: Routes = [
  { path: '',
    redirectTo: '/persons',
    pathMatch: 'full'
  },
  { path: 'persons',
    component: PersonsComponent,
    resolve: [PersonsResolverService, RolesResolverService, AuthResolverService],
    children: [
      { path: '',
        component: PersonStartComponent
      },
      { path:  'new',
        component: PersonEditComponent
      },
      { path: ':id',
        component: PersonDetailComponent,
        canActivate: [PersonIndexGuard]
      },
      { path:  ':id/edit',
        component: PersonEditComponent,
        canActivate: [PersonIndexGuard, IsHimGuard]
      }
    ]
  },
  { path: 'roles',
    component: RolesComponent,
    canActivate: [AdminGuard],
    resolve: [RolesResolverService, AuthResolverService],
    children: [
      { path:  'new',
        component: RoleEditComponent
      },
      { path: ':index',
        component: RoleEditComponent,
        canActivate: [RoleIndexGuard]
      }
    ]
  },
  { path: '**', redirectTo: '/persons' }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
  exports: [RouterModule]

})
export class AppRoutingModule {

}
