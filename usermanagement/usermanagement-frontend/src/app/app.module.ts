import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PersonsComponent } from './persons/persons.component';
import { PersonListComponent } from './persons/person-list/person-list.component';
import { RolesComponent } from './roles/roles.component';
import { RoleListComponent } from './roles/role-list/role-list.component';
import { PersonItemComponent } from './persons/person-list/person-item/person-item.component';
import { RoleItemComponent } from './roles/role-list/role-item/role-item.component';
import { HttpClientModule } from "@angular/common/http";
import { PersonService } from "./common/service/person.service";
import { PersonDetailComponent } from './persons/person-detail/person-detail.component';
import { PersonEditComponent } from './persons/person-edit/person-edit.component';
import { RoleEditComponent } from './roles/role-edit/role-edit.component';
import { PersonStartComponent } from './persons/person-start/person-start.component';
import { AppRoutingModule } from "./app-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RoleService } from "./common/service/role.service";
import {
  MatDatepickerModule, MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule
} from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DataStorageService } from "./common/service/data-storage.service";
import { PersonsResolverService } from "./common/service/resolver/persons-resolver.service";
import { RolesResolverService } from "./common/service/resolver/roles-resolver.service";
import { AuthService } from "./common/service/auth.service";
import { AuthResolverService } from "./common/service/resolver/auth-resolver.service";
import { AdminGuard } from "./common/service/guard/admin.guard";
import { IsHimGuard } from "./common/service/guard/is-him.guard";
import { PersonIndexGuard } from "./common/service/guard/person-index.guard";
import { RoleIndexGuard } from "./common/service/guard/role-index.guard";
import { ErrorDialogComponent } from './common/dialog/error-dialog/error-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PersonsComponent,
    PersonListComponent,
    RolesComponent,
    RoleListComponent,
    PersonItemComponent,
    RoleItemComponent,
    PersonDetailComponent,
    PersonEditComponent,
    RoleEditComponent,
    PersonStartComponent,
    ErrorDialogComponent
  ],
  entryComponents: [
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatOptionModule,
    MatDialogModule
  ],
  providers: [
    PersonService,
    RoleService,
    AuthService,
    DataStorageService,
    PersonsResolverService,
    RolesResolverService,
    AuthResolverService,
    AdminGuard,
    IsHimGuard,
    PersonIndexGuard,
    RoleIndexGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
