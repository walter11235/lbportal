export interface ErrorMessage {
  timestamp: Date;
  status: number;
  error: string;
  message: string;
  path: string;
}
