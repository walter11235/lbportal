import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { Person } from "../model/person.model";
import {catchError, map, tap} from "rxjs/operators";
import { PersonService } from "./person.service";
import { Injectable } from "@angular/core";
import { RoleService } from "./role.service";
import { Role } from "../model/role.model";
import {AuthService} from "./auth.service";
import {throwError} from "rxjs";

@Injectable()
export class DataStorageService {

  constructor(private httpClient: HttpClient,
              private roleService: RoleService,
              private personService: PersonService,
              private authService: AuthService) {}

  getPersons() {
    return this.httpClient.get<Person[]>("/api/usermanagement-backend/person/")
      .pipe(
        map(persons => {
          return persons.map(person => {
            return {...person, roles: person.roles ? person.roles : []};
          });
        }),
        tap(persons => {
          this.personService.setPersons(persons);
        }),
        catchError(this.handleError)
      )
  }

  updatePerson(index: number, person: Person) {
    return this.httpClient.post<Person>("/api/usermanagement-backend/person/save", person)
      .pipe(
        tap(person => {
        this.personService.updatePerson(index, person);
      }),
        catchError(this.handleError)
      )
  }

  addPerson(person: Person) {
    return this.httpClient.post<Person>("/api/usermanagement-backend/person/save", person)
      .pipe(
        tap(person => {
          this.personService.addPerson(person);
        }),
        catchError(this.handleError)
      )
  }

  deletePerson(index: number, person: Person) {
    return this.httpClient.delete("/api/usermanagement-backend/person/delete/" + person.id)
      .pipe(
        tap(person => {
          this.personService.deletePerson(index);
        }),
        catchError(this.handleError)
      )
  }

  getRoles() {
    return this.httpClient.get<Role[]>("/api/usermanagement-backend/role/")
      .pipe(
        tap(
          roles => {
            this.roleService.setRoles(roles);
          }),
        catchError(this.handleError)
      )
  }

  updateRole(index: number, role: Role) {
    return this.httpClient.post<Role>("/api/usermanagement-backend/role/save", role)
      .pipe(
        tap(role => {
          this.roleService.updateRole(index, role);
        }),
        catchError(this.handleError)
      )
  }

  deleteRole(index: number, role: Role) {
    return this.httpClient.delete<Role>("/api/usermanagement-backend/role/delete/" + role.id)
      .pipe(
        tap(role => {
            this.roleService.deleteRole(index);
            }),
        catchError(this.handleError)
      )
  }

  addRole(role: Role) {
    return this.httpClient.post<Role>("/api/usermanagement-backend/role/save", role)
      .pipe(
        tap(role => {
          this.roleService.addRole(role);
        }),
        catchError(this.handleError)
      )
  }

  getLoggedInUser() {
    return this.httpClient.get<Person>("/api/usermanagement-backend/person/loggedin_person")
      .pipe(
        tap( person => {
          this.authService.setLoggedInPerson(person);
        }),
        catchError(this.handleError)
      )
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = "An unknown error occurred!";
    if (!errorRes.error || !errorRes.error.error) {
      return throwError(errorMessage);
    }
    errorMessage = errorRes.error;
    return throwError(errorMessage);
  }

}
