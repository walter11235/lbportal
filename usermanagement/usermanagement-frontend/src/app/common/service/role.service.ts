import { Role } from "../model/role.model";
import {Subject} from "rxjs";

export class RoleService {

  private roles: Role[] = [];

  rolesChanged = new Subject<Role[]>();

  getRoles() {
    return this.roles.slice();
  }

  setRoles(roles: Role[]) {
    this.roles = roles;
    this.rolesChanged.next(this.roles.slice());
  }

  getRoleByIndex(index: number) {
    return this.roles[index];
  }

  addRole(role: Role) {
    this.roles.push(role);
    this.rolesChanged.next(this.roles.slice());
  }

  updateRole(index: number, role: Role) {
    this.roles[index] = role;
    this.rolesChanged.next(this.roles.slice());
  }

  deleteRole(index: number) {
    this.roles.splice(index, 1);
    this.rolesChanged.next(this.roles.slice());
  }

}
