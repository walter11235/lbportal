import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { DataStorageService } from "../data-storage.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { RoleService } from "../role.service";

@Injectable()
export class RoleIndexGuard implements CanActivate {
  constructor(private roleService: RoleService,
              private dataStorageService: DataStorageService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //to confirm if personResolver has already been invoked
    const currentIndex = route.params.index;
    const roles = this.roleService.getRoles();

    if(roles.length === 0) {
      return this.dataStorageService.getRoles()
        .pipe(
          map(roles => {
            return roles.length >= currentIndex;
          })
        )
    } else return !!this.roleService.getRoleByIndex(currentIndex);
  }
}
