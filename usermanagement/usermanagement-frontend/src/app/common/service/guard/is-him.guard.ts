import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from "@angular/router";
import { AuthService } from "../auth.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PersonService } from "../person.service";
import {DataStorageService} from "../data-storage.service";
import {exhaustMap, map} from "rxjs/operators";

@Injectable()
export class IsHimGuard implements CanActivate {
  constructor(private authService: AuthService,
              private dataStorageService: DataStorageService,
              private personService: PersonService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const currentIndex = route.params.id;
    //to confirm if the personResolver and authResolver have been already invoked
    const loggedInPerson = this.authService.getLoggedInPerson();
    const person = this.personService.getPersonByIndex(currentIndex);


    if(!loggedInPerson && !person) {
    return this.dataStorageService.getLoggedInUser().pipe(
      exhaustMap(loggedInPerson => {
        return this.dataStorageService.getPersons()
          .pipe(
            map(
              persons => {
                const person = this.personService.getPersonByIndex(currentIndex);
                return this.authService.isHim(person) || this.authService.isAdmin();
              })
          )
      })
    );
    } else {
      const person = this.personService.getPersonByIndex(currentIndex);
      return this.authService.isHim(person) || this.authService.isAdmin();
    }
  }
}
