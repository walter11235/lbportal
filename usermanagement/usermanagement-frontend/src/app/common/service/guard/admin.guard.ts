import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from "@angular/router";
import {AuthService} from "../auth.service";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {DataStorageService} from "../data-storage.service";
import {map} from "rxjs/operators";

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private authService: AuthService,
              private dataStorageService: DataStorageService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //to confirm if authResolver has already been invoked
    const loggedInPerson = this.authService.getLoggedInPerson();
    if (!loggedInPerson) {
      return this.dataStorageService.getLoggedInUser()
        .pipe(
          map(loggedInPerson => {
            return this.authService.isAdmin();
          })
        );
    } else {
      return this.authService.isAdmin();
    }
  }
}
