import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree } from "@angular/router";
import { DataStorageService } from "../data-storage.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { PersonService } from "../person.service";

@Injectable()
export class PersonIndexGuard implements CanActivate {
  constructor(private personService: PersonService,
              private dataStorageService: DataStorageService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //to confirm if personResolver has already been invoked
    const currentIndex = route.params.id;
    const persons = this.personService.getPersons();

    if(persons.length === 0) {
      return this.dataStorageService.getPersons()
        .pipe(
          map(persons => {
            return persons.length >= currentIndex;
          })
        )
    } else return !!this.personService.getPersonByIndex(currentIndex);
  }
}
