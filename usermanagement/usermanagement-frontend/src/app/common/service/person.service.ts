import { Person } from "../model/person.model";
import {Subject} from "rxjs";

export class PersonService {

  private persons: Person[] = [];

  personsChanged = new Subject<Person[]>();

  getPersons() {
    return this.persons.slice();
  }

  setPersons(persons: Person[]) {
    this.persons = persons;
    this.personsChanged.next(this.persons.slice());
  }

  getPersonByIndex(index: number) {
    return this.persons[index];
  }

  addPerson(person: Person) {
    this.persons.push(person);
    this.personsChanged.next(this.persons.slice());
  }

  updatePerson(index: number, updatedPerson: Person) {
    this.persons[index] = updatedPerson;
    this.personsChanged.next(this.persons.slice());
  }

  deletePerson(index: number) {
    this.persons.splice(index, 1);
    this.personsChanged.next(this.persons.slice());
  }

}
