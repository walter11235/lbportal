import { Person } from "../../model/person.model";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import {Observable} from "rxjs";
import {PersonService} from "../person.service";
import {DataStorageService} from "../data-storage.service";

@Injectable()
export class PersonsResolverService implements Resolve<Person[]> {
  constructor(private personService: PersonService,
              private dataStorageService: DataStorageService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Person[]> | Promise<Person[]> | Person[] {
    const persons = this.personService.getPersons();

    if(persons.length === 0) {
    return this.dataStorageService.getPersons();
    } else {
      return persons;
    }
  }


}
