import { Role } from "../../model/role.model";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {RoleService} from "../role.service";
import {DataStorageService} from "../data-storage.service";
import {Injectable} from "@angular/core";

@Injectable()
export class RolesResolverService implements Resolve<Role[]> {

  constructor(private roleService: RoleService,
              private dataStorageService: DataStorageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Role[]> | Promise<Role[]> | Role[] {

    const roles = this.roleService.getRoles();

    if (roles.length === 0) {
      return this.dataStorageService.getRoles();
    } else {
      return roles;
    }

  }


}
