import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Person } from "../../model/person.model";
import { Observable } from "rxjs";
import {AuthService} from "../auth.service";
import {DataStorageService} from "../data-storage.service";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthResolverService implements Resolve<Person> {

  constructor(private authService: AuthService,
              private dataStorageService: DataStorageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Person> | Promise<Person> | Person {
    const loggedInPerson = this.authService.getLoggedInPerson();

    if(loggedInPerson === null) {
      return this.dataStorageService.getLoggedInUser();
    } else {
      return loggedInPerson;
    }
  }

}
