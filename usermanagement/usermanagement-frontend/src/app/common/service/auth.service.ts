import {Person} from "../model/person.model";
import {Subject} from "rxjs";

export class AuthService {

  private loggedInPerson: Person = null;

  loggedInPersonSet = new Subject<Person>();

  getLoggedInPerson() {
    return this.loggedInPerson;
  }

  setLoggedInPerson(loggedInPerson: Person) {
    this.loggedInPerson = loggedInPerson;
    this.loggedInPersonSet.next(this.loggedInPerson);
  }

  isAdmin() {
    let isAdmin = false;
    this.loggedInPerson.roles.forEach(role => {
      if (role.name === "admin") {isAdmin = true;}
    });
    return isAdmin;
  }

  isHim(person: Person) {
    if (this.loggedInPerson.emailAddress === person.emailAddress) {
      return true;
    }
  }
}
