import { Component, Input, OnInit } from '@angular/core';
import { Role } from "../../../common/model/role.model";

@Component({
  selector: 'app-role-item',
  templateUrl: './role-item.component.html',
  styleUrls: ['./role-item.component.css']
})
export class RoleItemComponent implements OnInit {

  @Input() role: Role;

  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

}
