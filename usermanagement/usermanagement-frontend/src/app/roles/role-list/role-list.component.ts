import { Component, OnDestroy, OnInit } from '@angular/core';
import { RoleService } from "../../common/service/role.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Role } from "../../common/model/role.model";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit, OnDestroy {

  roles: Role[] = [];

  subscription: Subscription;

  constructor(private roleService: RoleService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.subscription = this.roleService.rolesChanged
      .subscribe((roles: Role[]) => {
        this.roles = roles;
      });
    this.roles = this.roleService.getRoles();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onNew() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
