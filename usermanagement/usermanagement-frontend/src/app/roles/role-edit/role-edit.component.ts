import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {DataStorageService} from "../../common/service/data-storage.service";
import {RoleService} from "../../common/service/role.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Role} from "../../common/model/role.model";
import {ErrorMessage} from "../../common/dialog/error-dialog/error-message.interface";
import {ErrorDialogComponent} from "../../common/dialog/error-dialog/error-dialog.component";
import {Observable} from "rxjs";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.css']
})
export class RoleEditComponent implements OnInit {

  @ViewChild('f', {static: true}) roleForm: NgForm;

  index: number;
  role: Role;
  editMode: boolean = false;

  constructor(private roleService: RoleService,
              private dataStorageService: DataStorageService,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.index = +params['index'];
        this.editMode = params['index'] != null;
        if (this.editMode) {
        this.role = this.roleService.getRoleByIndex(this.index);
        } else {
          this.role = new Role();
        }
      })
  }

  onSubmit() {
    this.role.name = this.roleForm.value.name;
    let observable : Observable<any>;
    if (this.editMode) {
      observable = this.dataStorageService.updateRole(this.index, this.role);
    } else {
      observable = this.dataStorageService.addRole(this.role);
    }
    observable.subscribe(response => {this.onCancel();},
      errorMessage => {this.openErrorDialog(errorMessage);}

      )
  }

  private onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDelete() {
    this.dataStorageService.deleteRole(this.index, this.role)
      .subscribe(response => {this.onCancel();},
        errorMessage => {this.openErrorDialog(errorMessage);}

      )
  }

  openErrorDialog(errorMessage: ErrorMessage): void {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMessage,
      width: '500px',
      height: '250px'
    })
  }

}
