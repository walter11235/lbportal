package de.lumabit.lbportal.usermanagementbackend.service.implementation;

import de.lumabit.lbportal.usermanagementbackend.model.Address;
import de.lumabit.lbportal.usermanagementbackend.repository.AddressRepository;
import de.lumabit.lbportal.usermanagementbackend.service.AddressService;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Address save(Address address) {
        return addressRepository.save(address);
    }

    @Override
    public void delete(Address address) {
        addressRepository.delete(address);
    }

    @Override
    public void deleteById(Long id) {
        addressRepository.deleteById(id);
    }
}
