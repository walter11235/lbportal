package de.lumabit.lbportal.usermanagementbackend.service;

import de.lumabit.lbportal.usermanagementbackend.model.Role;

import java.util.Set;

public interface RoleService {

    Set<Role> findAll();

    Role save(Role role);

    void delete(Role role);

    Role findById(Long id);

    Role findByName(String name);

}
