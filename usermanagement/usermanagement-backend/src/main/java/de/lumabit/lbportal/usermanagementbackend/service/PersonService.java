package de.lumabit.lbportal.usermanagementbackend.service;

import de.lumabit.lbportal.usermanagementbackend.model.Person;

import java.util.Set;

public interface PersonService {

    Set<Person> findAll();

    Person findById(Long id);

    Person save(Person person);

    void delete(Person person);

    Person findByEmailAddress(String email);

    Person findByGoogleId(String googleId);
}
