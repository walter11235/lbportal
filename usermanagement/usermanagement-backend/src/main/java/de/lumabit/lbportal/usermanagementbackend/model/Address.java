package de.lumabit.lbportal.usermanagementbackend.model;

import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address extends BaseEntity {

    @NotBlank(message = "Address line cannot be blank or null")
    private String addressLine;

    @NotBlank(message = "state cannot be blank or null")
    private String state;

    @NotBlank(message = "city cannot be blank or null")
    private String city;

    @NotNull(message = "zipCode cannot be null")
    private Integer zipCode;

}
