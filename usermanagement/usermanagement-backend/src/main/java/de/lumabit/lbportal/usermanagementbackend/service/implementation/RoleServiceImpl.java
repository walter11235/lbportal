package de.lumabit.lbportal.usermanagementbackend.service.implementation;

import de.lumabit.lbportal.usermanagementbackend.exception.NotFoundException;
import de.lumabit.lbportal.usermanagementbackend.model.Role;
import de.lumabit.lbportal.usermanagementbackend.repository.PersonRepository;
import de.lumabit.lbportal.usermanagementbackend.repository.RoleRepository;
import de.lumabit.lbportal.usermanagementbackend.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final PersonRepository personRepository;

    public RoleServiceImpl(RoleRepository roleRepository, PersonRepository personRepository) {
        this.roleRepository = roleRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Set<Role> findAll() {
        Set<Role> roles = new HashSet<>();
        roleRepository.findAll().forEach(roles::add);
        return roles;
    }

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public void delete(Role role) {
        role.getPersons().forEach((person) -> {
            person.getRoles().remove(role);
            personRepository.save(person);
        });
        roleRepository.delete(role);
    }

    @Override
    public Role findById(Long id) {
        return roleRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Role not found"));
    }

    @Override
    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }
}
