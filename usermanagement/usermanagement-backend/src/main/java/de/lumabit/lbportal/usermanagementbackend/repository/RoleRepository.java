package de.lumabit.lbportal.usermanagementbackend.repository;

import de.lumabit.lbportal.usermanagementbackend.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByName(String name);
}
