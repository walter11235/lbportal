package de.lumabit.lbportal.usermanagementbackend.loader;

import de.lumabit.lbportal.usermanagementbackend.model.Address;
import de.lumabit.lbportal.usermanagementbackend.model.Person;
import de.lumabit.lbportal.usermanagementbackend.model.Role;
import de.lumabit.lbportal.usermanagementbackend.service.AddressService;
import de.lumabit.lbportal.usermanagementbackend.service.PersonService;
import de.lumabit.lbportal.usermanagementbackend.service.RoleService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

@Component
public class DataLoader implements CommandLineRunner {

    private final PersonService personService;
    private final AddressService addressService;
    private final RoleService roleService;

    public DataLoader(PersonService personService, AddressService addressService, RoleService roleService) {
        this.personService = personService;
        this.addressService = addressService;
        this.roleService = roleService;
    }

    @Override
    public void run(String... args) throws Exception {
        Role role1 = Role.builder().name("admin").build();
        Role role2 = Role.builder().name("user").build();

        roleService.save(role1);
        roleService.save(role2);

        Address address1 = Address.builder()
                .addressLine("Schubertstr 1")
                .city("Erlangen")
                .state("Bayern")
                .zipCode(91052)
                .build();

        Address address2 = Address.builder()
                .addressLine("Schubertstr 2")
                .city("Erlangen")
                .state("Bayern")
                .zipCode(91052)
                .build();

        Address address3 = Address.builder()
                .addressLine("Schubertstr 3")
                .city("Erlangen")
                .state("Bayern")
                .zipCode(91052)
                .build();

        Person person1 = Person.builder()
                .address(address1)
                .birthDate(new Date())
                .emailAddress("vijayamohan@lumabit.de")
                .firstName("Harish")
                .lastName("Vijayamohan")
                .roles(new HashSet<>(Arrays.asList(role1, role2)))
                .build();

        Person person2 = Person.builder()
                .address(address2)
                .birthDate(new Date())
                .emailAddress("huber@lumabit.de")
                .firstName("Tobias")
                .lastName("Huber")
                .roles(new HashSet<>(Arrays.asList(role1, role2)))
                .build();

        Person person3 = Person.builder()
                .address(address3)
                .birthDate(new Date())
                .emailAddress("nguyen@lumabit.de")
                .firstName("Hai")
                .lastName("Nguyen")
                .roles(new HashSet<>(Arrays.asList(role1, role2)))
                .build();


        personService.save(person1);
        personService.save(person2);
        personService.save(person3);
    }
}
