package de.lumabit.lbportal.usermanagementbackend.repository;

import de.lumabit.lbportal.usermanagementbackend.model.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PersonRepository extends CrudRepository<Person, Long> {
Optional<Person> findByEmailAddress(String email);
Optional<Person> findByGoogleId(String googleId);
}
