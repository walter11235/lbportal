package de.lumabit.lbportal.usermanagementbackend.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Person extends BaseEntity {

    //can be applied only to text values and validated
    // that the property is not null or whitespace
    @NotBlank(message = "FirstName cannot be Blank or Null")
    private String firstName;

    @NotBlank(message = "LastName cannot be Blank or Null")
    private String lastName;

    @NotEmpty(message = "Roles cannot be Null or Empty")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "person_role",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
    
    //stores date without time like "yyyy-MM-dd"
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @Column(unique=true)
    @Email(message = "Email should be valid")
    private String emailAddress;

    //Validates that the property is not null or empty;
    //It can be applied to String, Collection, Map or Array values
    @NotNull(message = "Address cannot be null")
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    private String imageUrl;

    private String googleId;
}
