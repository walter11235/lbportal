package de.lumabit.lbportal.usermanagementbackend.controller;

import de.lumabit.lbportal.usermanagementbackend.model.Person;
import de.lumabit.lbportal.usermanagementbackend.service.PersonService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping("/person")
public class PersonController {
    private final PersonService personService;


    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public Principal getPrincipal(Principal principal) {
        //System.out.println(principal.getName());
        return principal;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Set<Person> getAll() {
        return personService.findAll();
    }

    @RequestMapping(value = "/loggedin_person", method = RequestMethod.GET)
    public Person getCurrentUser(Principal principal) {
        return personService.findByGoogleId(principal.getName());
    }

    //expression based access control
    @PreAuthorize("hasAuthority('admin') or #person.googleId == authentication.name")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Person save(@RequestBody Person person) {
        return personService.save(person);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Person getPersonById(@PathVariable Long id) {
        return personService.findById(id);
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable Long id) {
        Person person = personService.findById(id);
        personService.delete(person);
    }
}
