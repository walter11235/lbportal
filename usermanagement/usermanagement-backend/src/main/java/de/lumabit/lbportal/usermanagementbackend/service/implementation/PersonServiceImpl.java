package de.lumabit.lbportal.usermanagementbackend.service.implementation;

import de.lumabit.lbportal.usermanagementbackend.exception.NotFoundException;
import de.lumabit.lbportal.usermanagementbackend.model.Person;
import de.lumabit.lbportal.usermanagementbackend.repository.PersonRepository;
import de.lumabit.lbportal.usermanagementbackend.service.PersonService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;


    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Set<Person> findAll() {
        Set<Person> persons = new HashSet<>();
        personRepository.findAll().forEach(persons::add);
        return persons;
    }

    @Override
    public Person findById(Long id) {
        Optional<Person> person = personRepository.findById(id);
        return person.orElseThrow(() -> new NotFoundException("Person Not Found"));
    }

    @Override
    public Person save(Person person) {
        return personRepository.save(person);
    }

    @Override
    @PreAuthorize("hasAuthority('admin') or #person.googleId == authentication.name")
    public void delete(Person person) {
        personRepository.delete(person);
    }

    @Override
    public Person findByEmailAddress(String email) {
        return personRepository.findByEmailAddress(email).orElseThrow(() -> new NotFoundException("Person Not Found"));
    }

    @Override
    public Person findByGoogleId(String googleId) {
        return personRepository.findByGoogleId(googleId).orElseThrow(() -> new NotFoundException("Person Not Found"));
    }
}
