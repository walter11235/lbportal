package de.lumabit.lbportal.usermanagementbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UsermanagementBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsermanagementBackendApplication.class, args);
    }

}
