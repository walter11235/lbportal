package de.lumabit.lbportal.usermanagementbackend.config;

import de.lumabit.lbportal.usermanagementbackend.exception.NotFoundException;
import de.lumabit.lbportal.usermanagementbackend.model.Address;
import de.lumabit.lbportal.usermanagementbackend.model.Person;
import de.lumabit.lbportal.usermanagementbackend.model.Role;
import de.lumabit.lbportal.usermanagementbackend.service.PersonService;
import de.lumabit.lbportal.usermanagementbackend.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

public class CustomAuthoritiesExtractor implements AuthoritiesExtractor {

    @Autowired
    private PersonService personService;

    @Autowired
    private RoleService roleService;

    @Override
    public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        Person person;
        try {

            person = personService.findByEmailAddress((String)map.get("email"));
            person.setImageUrl((String)map.get("picture"));
            person.setGoogleId((String)map.get("id"));
            person = personService.save(person);

        } catch (NotFoundException e) {

            Address address = Address.builder()
                    .addressLine("Street Name")
                    .city("City Name")
                    .state("State Name")
                    .zipCode(0)
                    .build();

            Person newPerson = Person.builder()
                    .emailAddress((String)map.get("email"))
                    .firstName((String)map.get("given_name"))
                    .lastName((String)map.get("family_name"))
                    .imageUrl((String)map.get("picture"))
                    .googleId((String)map.get("id"))
                    .address(address)
                    .birthDate(new Date())
                    .roles(new HashSet<>(Arrays.asList(roleService.findByName("user"))))
                    .build();
            person = personService.save(newPerson);
        }

        Set<Role> roles = person.getRoles();
        for (Role role: roles) {
            authorities.add( new SimpleGrantedAuthority(role.getName()) );
        }
        return authorities;
    }
}
