package de.lumabit.lbportal.usermanagementbackend.service;

import de.lumabit.lbportal.usermanagementbackend.model.Address;

public interface AddressService {

    Address save(Address address);
    void delete(Address address);
    void deleteById(Long id);
}
