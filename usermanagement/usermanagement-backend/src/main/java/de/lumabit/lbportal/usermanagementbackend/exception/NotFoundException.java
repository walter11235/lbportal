package de.lumabit.lbportal.usermanagementbackend.exception;


public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}
