package de.lumabit.lbportal.usermanagementbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BaseEntity {

    //Validates that the property is not null or empty;
    //It can be applied to String, Collection, Map or Array values
    @NotEmpty(message = "Role name cannot be Null or Empty")
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    @EqualsAndHashCode.Exclude
    private Set<Person> persons;

}
