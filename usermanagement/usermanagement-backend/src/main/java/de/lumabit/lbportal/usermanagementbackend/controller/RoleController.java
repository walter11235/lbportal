package de.lumabit.lbportal.usermanagementbackend.controller;

import de.lumabit.lbportal.usermanagementbackend.model.Role;
import de.lumabit.lbportal.usermanagementbackend.service.RoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/role")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Set<Role> getRoles() {
        return roleService.findAll();
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Role updateRole(@RequestBody Role role) {
        return this.roleService.save(role);
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteRoleById(@PathVariable Long id) {
        Role role = this.roleService.findById(id);
        this.roleService.delete(role);
    }
}
