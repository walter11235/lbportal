package de.lumabit.lbportal.usermanagementbackend.repository;

import de.lumabit.lbportal.usermanagementbackend.model.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
}
