package de.lumabit.lbportal.usermanagementbackend.repository;


import de.lumabit.lbportal.usermanagementbackend.exception.NotFoundException;
import de.lumabit.lbportal.usermanagementbackend.model.Address;
import de.lumabit.lbportal.usermanagementbackend.model.Person;
import de.lumabit.lbportal.usermanagementbackend.model.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void findAllShouldReturnPersons() {
        Role role1 = Role.builder().name("admin").build();
        Role role2 = Role.builder().name("user").build();
        Address address1 = Address.builder()
                .addressLine("Schubertstr 1")
                .city("Erlangen")
                .state("Bayern")
                .zipCode(91052)
                .build();

        Address address2 = Address.builder()
                .addressLine("Schubertstr 2")
                .city("Erlangen")
                .state("Bayern")
                .zipCode(91052)
                .build();

        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(address1)
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList(role1, role2)))
                .build();

        Person person2 = Person.builder().firstName("Huber")
                .lastName("Tobias")
                .birthDate(new Date())
                .address(address2)
                .emailAddress("huber@lumabit.de")
                .roles(new HashSet<>(Arrays.asList(role1, role2)))
                .build();

        this.entityManager.persist(person1);
        this.entityManager.persist(person2);

        Assertions.assertEquals(2L, personRepository.count());
    }

    @Test
    public void findByIdShouldReturnPerson() {
        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(Address.builder()
                        .addressLine("Schubertstr 2")
                        .city("Erlangen")
                        .state("Bayern")
                        .zipCode(91052)
                        .build())
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList( Role.builder().name("admin").build())))
                .build();
        this.entityManager.persist(person1);
        Assertions.assertEquals(person1, personRepository.findById(person1.getId()).orElse(null));
    }

    @Test
    public void findByIdShouldReturnNull() {
        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(Address.builder()
                        .addressLine("Schubertstr 2")
                        .city("Erlangen")
                        .state("Bayern")
                        .zipCode(91052)
                        .build())
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList( Role.builder().name("admin").build())))
                .build();
        this.entityManager.persist(person1);
        Assertions.assertNull(personRepository.findById(000L).orElse(null));
    }

    @Test
    public void findByIdShouldThrowException() {
        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(Address.builder()
                        .addressLine("Schubertstr 2")
                        .city("Erlangen")
                        .state("Bayern")
                        .zipCode(91052)
                        .build())
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList( Role.builder().name("admin").build())))
                .build();
        this.entityManager.persist(person1);
        Optional<Person> person = personRepository.findById(00L);
        Assertions.assertThrows(NotFoundException.class, () ->
                person.orElseThrow(() -> new NotFoundException("Person Not Found"))
        );
    }

    @Test
    public void saveShouldReturnPerson() {
        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(Address.builder()
                        .addressLine("Schubertstr 2")
                        .city("Erlangen")
                        .state("Bayern")
                        .zipCode(91052)
                        .build())
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList( Role.builder().name("admin").build())))
                .build();

        Assertions.assertEquals(person1, personRepository.save(person1));
    }

    @Test
    public void deleteShouldRemovePerson() {
        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(Address.builder()
                        .addressLine("Schubertstr 2")
                        .city("Erlangen")
                        .state("Bayern")
                        .zipCode(91052)
                        .build())
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList( Role.builder().name("admin").build())))
                .build();

        this.entityManager.persist(person1);

        personRepository.delete(person1);

        Assertions.assertNull(personRepository.findById(person1.getId()).orElse(null));
    }

    @Test
    public void deletebyIdShouldRemovePerson() {
        Person person1 = Person.builder().firstName("Harish")
                .lastName("Vijayamohan")
                .birthDate(new Date())
                .address(Address.builder()
                        .addressLine("Schubertstr 2")
                        .city("Erlangen")
                        .state("Bayern")
                        .zipCode(91052)
                        .build())
                .emailAddress("vijayamohan@lumabit.de")
                .roles(new HashSet<>(Arrays.asList( Role.builder().name("admin").build())))
                .build();

        this.entityManager.persist(person1);

        personRepository.deleteById(person1.getId());

        Assertions.assertNull(personRepository.findById(person1.getId()).orElse(null));
    }
}
